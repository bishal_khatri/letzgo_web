<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Roles extends Model
{

	public function users()
	{
		return $this->belongsToMany(User::class, 'role_user');
	}

	 public function hasAccess(array $permissions)
    {
     
        foreach ($permissions as $permission) {
            if ($this->hasPermission($permission)) {
                return true;
            }          
        }
        return false;
    }

    protected function hasPermission(string $permission)
    {
    	$permissions = json_decode($this->permissions,true);
    	return $permissions[$permission]??false;
    }
 
}
