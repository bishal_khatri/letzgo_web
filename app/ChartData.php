<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartData extends Model
{
    protected $connection= 'mysql-2';

    public function data_list()
    {
    	$data = \DB::connection('mysql-2')->table('devices as d')->join('positions as p','p.deviceid','d.id')->where('d.id',219)->get();
    	return $data;
    }

    public function get_first_date()
    {
    	$data = \DB::connection('mysql-2')->table('positions')->where('deviceid',219)->orderBy('id')->first();
    	// dd($data);
    	return $data;
    }
}
