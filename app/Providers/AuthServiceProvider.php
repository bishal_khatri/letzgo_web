<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();
    }

    public function registerUserPolicies()
    {
        Gate::before(function($user, $ability) {
            if($user->security_key == 'superadmin') {
                return true;
            }
        });
        if (Schema::hasTable('permissions')) {
            $permissions = Permission::all(['slug'])->toArray();
            // dd($permissions);
            foreach ($permissions as $permission) {
                $roles = $permission['slug'];
                // dd($roles);
                if (!empty($roles))
                {
                    Gate::define($roles, function($user) use($roles) {
                        return $user->hasAccess([$roles]);
                    });
                }
            }
        }
    }


}
