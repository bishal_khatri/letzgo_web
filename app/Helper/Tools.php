<?php
namespace App\Helper;

use App\Module;
use App\Menu;
use Auth;
use DB;
use App\Permission;
use Illuminate\Http\Request;
use Debugbar;

/**
*
*/
class Tools
{

	public static function getFlashMessage($msgType, $content)
	{
		$messageType = "alert-" . $msgType;
		if ($msgType=='success') {
			$icon = 'fa fa-check';
		}elseif ($msgType=='danger') {
			$icon = 'fa fa-exclamation-triangle';
		}

		return '
					<div id="alert" style="border:1px solid;" class="alert ' . $messageType . '">
					<i class="'.$icon.'"
					style="
						background-color:#fff;
						border:1px #000;
						border-radius: 10px;
						" aria-hidden="true"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							</button><span style="padding-left:15px;letter-spacing: 3px;color:#303030;">'.$content . '
					</span></div>';

	}

	public static function random_keys($length)
    {
        $string = '';
        $characters = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWKYZ";
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    public static function getPermissions()
    {
    	$permission = Permission::all(['slug'])->toArray();
    	return $permission;
    }

    public static function setResponse($type,$message,$data,$meta)
    {
        if ($type=='success') {
            $error = false;
        }
        elseif($type=='fail'){
            $error = true;
        }

        $responseData = [
            'error' => $error,
            'message'=>$message,
            'data' => $data,
            'meta' => $meta
        ];
        return $responseData;

    }

    // public function validate($rule,$data)
    // {

    // }

}
