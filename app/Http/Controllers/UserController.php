<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Image;
use App\User;
use App\Roles;
use App\Helper\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{

    // start of users
    public function showRegistrationForm()
    {
        $data['main_menu_name'] = 'User';
        $data['page_title'] = 'add';
        $data['roles'] = Roles::all();
        return view('auth.register',$data);

    }

    public function store(Request $request)
    {
      // dd($request->all());
        $rolesId = $request->roles;
        $register_ip = \Request::ip();
        $this->validate($request, [
            'full_name' => 'required|string|max:255',
            'user_name' => 'required|max:255',
            'password' => 'required|confirmed|min:6'
        ]);
        $user = new User();
        $checkUserName = User::where('user_name',$request['user_name'])->count();
        $checkUserEmail = User::where('email',$request['email'])->count();
        if ($checkUserName>0) {
            $flashMessage = Tools::getFlashMessage('danger', 'Sorry Username "'.$request['user_name'].'" already exists. Please try another.');
            \Session::flash('flash_message', $flashMessage);
            return redirect()->back();
        }
        // elseif ($checkUserEmail>1) {
        //   $flashMessage = Tools::getFlashMessage('danger', 'Sorry Email "'.$request['email'].'" already exists. Please try another.');
        //   \Session::flash('flash_message', $flashMessage);
        //   return redirect()->back();
        // }
        else{
            if (Input::hasFile('image')) {
                $file = Input::file('image');;
                $ext = $file->getClientOriginalExtension();
                $image_name = date('YmdHis') . "." . $file->getClientOriginalExtension();
                $destination_path = 'public/uploads/profile/'. $image_name;
                $img = Image::make($file->getRealPath());
                $img->save($destination_path);
            }

            $user->full_name = $request['full_name'];
            if (isset($image_name)) {
            $user->profile_image = $image_name;
            }
            $user->email = $request['email'];
            $user->phone = $request['phone'];
            $user->address = $request['address'];
            $user->city = $request['city'];
            $user->user_name = $request['user_name'];
            $user->password = bcrypt($request['password']);
            $user->real_password = $request['password'];
            $user->register_ip = $register_ip;
            $user->active = '1';
            $user->security_key = str_random(40);
            $user->save();
            $user->roles()->attach($rolesId);

            $flashMessage = Tools::getFlashMessage('success', 'Added "'.$request['full_name'].'" successfully');
            \Session::flash('flash_message', $flashMessage);
            return redirect()->route('user.list');
        }
    }

    public function listUser()
    {
        $data['main_menu_name'] = 'User';
        $data['page_title'] = 'list';
        if (Auth::user()->security_key=='superadmin') {
            $data['users'] = User::where('id','!=',1)->get();
        }
        else{
            $data['users'] = User::where('id','!=',1)->where('airlines_id',Auth::user()->airlines_id)->get();
        }
        return view('users.list',$data);
    }

    public function getEdit($key)
    {
        $data['data'] = User::where('security_key','=',$key)->first();
        $data['main_menu_name'] = 'User';
        $data['page_title'] = 'Update';
        return view('users.edit',$data);
    }

    public function update(Request $request, $key)
    {
        $user = User::where('security_key','=',$key)->first();

        $register_ip = \Request::ip();
        $this->validate($request, [
            'full_name' => 'required|string|max:255',
        ]);

        $user->full_name = $request['full_name'];
        $user->email = $request['email'];
        $user->phone = $request['phone'];
        $user->address = $request['address'];
        $user->city = $request['city'];
        $user->register_ip = $register_ip;
        $user->save();

        $data['main_menu_name'] = 'User';
        $data['page_title'] = 'Update';
        return redirect()->route('user.list');
    }

    public function delete(Request $request)
    {
        $user = User::where('security_key','=',$request->id)->first();
        $user->delete();
        $flashMessage = Tools::getFlashMessage('success', 'Deleted "'.$user->full_name.'" successfully');
        \Session::flash('flash_message', $flashMessage);
        return null;
    }

    public function suspend(Request $request)
    {
        $user = User::where('security_key','=',$request->id)->first();
        $user->active = 0;
        $user->save();
        $flashMessage = Tools::getFlashMessage('success', 'Suspended "'.$user->full_name.'" successfully');
        \Session::flash('flash_message', $flashMessage);
        return null;
    }

    public function restore(Request $request)
    {
        $user = User::where('security_key','=',$request->id)->first();
        $user->active = 1;
        $user->save();
        $flashMessage = Tools::getFlashMessage('success', 'Restored "'.$user->full_name.'" successfully');
        \Session::flash('flash_message', $flashMessage);
        return null;
    }

    // end of users
}
