<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use App\Permission;
use App\Helper\Tools;

class RolesController extends Controller
{
    public function index()
    {
    	$data['main_menu_name'] = 'User';
    	$data['page_title'] = 'Roles';
        $data['roles'] = Roles::all();
    	return view('roles.index',$data);
    }

    public function showRolesFrom()
    {
    	$data['main_menu_name'] = 'User';
    	$data['page_title'] = 'Roles - Add';
    	return view('roles.addRoles',$data);
    }

    public function showRolesEditFrom($id)
    {
        $data['main_menu_name'] = 'User';
        $data['page_title'] = 'Roles - Edit';
        $roles = $data['roles'] = Roles::find($id);
        $permissions = json_decode($roles->permissions,true); // get permissions form in associative array form from roles object
        // dd($permissions);
        // populating permission array if empty
        if ($permissions==null) {
            $permissions = array(
                "permission" => false,
            );
        }
        $data['perm'] = array_keys($permissions); // gets only key from the array
        $data['permissions'] = Permission::all();
        return view('roles.rolesEdit',$data);
    }


    public function rolesStore(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required'
        ]);
    	$roles = new Roles();

    	$roles->name = $request['name'];
        $roles->slug = str_slug($request['name']);
    	$roles->security_key = Tools::random_keys(50);

    	$roles->save();
        $flashMessage = Tools::getFlashMessage('success', 'Added "'.$request['title'].'" successfully');
        \Session::flash('flash_message', $flashMessage);

    	return redirect()->route('role.list');
    }

    public function update(Request $request, $id)
    {
        // dd($request->permission);
        if (isset($request->permission)) {
            foreach ($request->permission as $value) {
                $json[] = [$value =>true];
            }
        }

        $result = array();
        if (isset($json)) {
            foreach ($json as $key => $value) { 
                if (is_array($value)) { 
                  $result = array_merge($result, array_merge($value)); 
                }
            } 
        }
        $permission = json_encode($result);
        // dd($permission);
        $role = Roles::find($id);
        $role->name=$request->roleName;
        $role->permissions=$permission;
        $role->save();

        $flashMessage = Tools::getFlashMessage('success', 'Updated "'.$request->roleName.'" successfully');
        \Session::flash('flash_message', $flashMessage);
        
        return redirect()->route('role.edit',$id);

    }

    public function delete(Request $request)
    {
       $key = $request->id;
       $role = Roles::where('security_key','=',$key)->first();
       $role->delete();
       $flashMessage = Tools::getFlashMessage('success', 'Deleted "'.$role->name.'" successfully');
        \Session::flash('flash_message', $flashMessage);
       return null;    
   }
}
