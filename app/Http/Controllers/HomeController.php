<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','lock']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu_name'] = 'Home';
        $data['page_title'] = 'Home';
        $data['users'] = User::where('security_key','!=','superadmin')->count();

        return view('home',$data);
    }

}
