<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\Menu;
use Debugbar;
use DB;
use App\Helper\Tools;
// use App\Permission;

class ModuleController extends Controller
{
   
	public function create()
	{
		$data['main_menu_name'] = 'Module';
		$data['page_title'] = 'Add';

		return view('modules.createModule', $data);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
            'display_name' => 'required|string',
            'name' => 'required|string|',
            'icon' => 'required',
            'order' => 'integer'
        ]);

        $module = new Module();

        $module->display_name = $request['display_name'];
        $module->name = $request['name'];
        $module->table_name = $request['table_name'];
        $module->icon = $request['icon'];
        $module->order = $request['order'];
        $module->status = $request['status'];
        $module->security_key = Tools::random_keys(45);
        $module->is_single = $request['is_single'];
        $module->save();

        $flashMessage = Tools::getFlashMessage('success', 'Added "'.$module['display_name'].'" successfully');
        \Session::flash('flash_message', $flashMessage);

        return redirect()->route('module.list');

	}

    public static function listMenu()
    {
        
      $menuArray = array();
        $menus = Module::join('menus', 'modules.id', '=', 'menus.module_id')
            ->selectRaw('modules.display_name,modules.name,menus.menu_name,menu_icon,modules.icon,menus.module_link,menus.link,menu_order,modules.is_single, menus.slug')
            ->where('status', 1)
            ->where('show_hide', 1)
            ->orderBy('order', 'ASC')
            ->orderBy('menu_order', 'ASC')
            ->get();

        foreach ($menus as $key => $menu) {
                $menuArray[$menu->name . '_' . $menu->icon . '_' . $menu->is_single][] = $menu->toArray();

            }
        
        // Debugbar::info($menuArray);
        // dd($menuArray);
        return $menuArray;
    }

    public function listModule()
    {
        $data['main_menu_name'] = 'Module';
        $data['page_title'] = 'List';

        $data['module'] = Module::all();

        return view('modules.listModule', $data);
    }

    public function createMenu($id,$name)
    {
        $data['main_menu_name'] = 'Menu';
        $data['page_title'] = 'Add';
        $data['id'] = $id;
        $data['name'] = $name;


        return view('modules.menu', $data);

    }

    public function viewMenu($id)
    {
        // dd($id);
        $data['main_menu_name'] = 'Menu';
        $data['page_title'] = 'Menu-View';
        $data['id'] = $id;

        $data['menu'] = Menu::where('module_id','=',$id)->orderBy('menu_order','ASC')->get(); 
        // dd($data['menu']);
        return view('modules.menuView', $data);

    }

    public function storeMenu(Request $request)
    {
       // dd($request->all());
        $this->validate($request, [
            'menu_name' => 'required',
            'menu_icon' => 'required',
            'module_link' => 'required',
            'module_id' => 'required',
            'link' => 'required'
        ]);
         // dd($request->all());

        $module = new Menu();

        $module->module_id = $request['module_id'];
        $module->menu_name = $request['menu_name'];
        $module->menu_icon = $request['menu_icon'];
        $module->module_link = $request['module_link'];
        $module->link = $request['link'];
        $module->slug = $request['slug'];
        $module->menu_order = $request['menu_order'];
        $module->show_hide = $request['show_hide'];
        // $module->parent_id = $request['parent_id'];
        $module->save();

        // save permission
        // $permission = new Permission();

        $flashMessage = Tools::getFlashMessage('success', 'Added "'.$module['menu_name'].'" successfully');
        \Session::flash('flash_message', $flashMessage);

        return redirect()->route('module.list');
        // dd($request->all());
    }

    // edit module
    public function editModuleForm($key)
    {
        $data['main_menu_name'] = 'Module';
        $data['page_title'] = 'Edit';
        $data['data'] = Module::where('security_key',$key)->first();
        return view('modules.editModule', $data);
    }
    // edit form module end

    // update module
    public function updateModule($key)
    {
        dd($key);
    }

    public function deleteModule(Request $request)
    {
        $module = Module::where('security_key','=',$request->id)->first();
        $module->delete();
        $flashMessage = Tools::getFlashMessage('success', 'Deleted "'.$module->name.'" successfully');
        \Session::flash('flash_message', $flashMessage);
       return null;            
    }

}
