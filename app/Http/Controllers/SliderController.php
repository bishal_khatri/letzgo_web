<?php

namespace App\Http\Controllers;

use File;
use Auth;
use Image;
use App\Slider;
use App\Helper\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SliderController extends Controller
{
    /**
     * SliderController constructor.
     */
    public function __construct()
    {
    }

    public function index()
    {
      $data['main_menu_name'] = 'Slider';
      $data['page_title'] = 'List';
      $data['slider'] = Slider::all();
      return view('slider.index',$data);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'image' => 'required'
        ]);
        $slider = new Slider();
        if (Input::has('image')) {
          $file = Input::file('image');
          $ext = $file->getClientOriginalExtension();
          $file_name = date('YmdHis') . "." . $ext;
          $destination_path = 'public/uploads/slider/';
          $full_name = $destination_path . $file_name;
          $request->file('image')->move($destination_path,$file_name);
          $slider->image = $full_name;
        }
        $slider->unique_key = str_random(45);
        $slider->save();
        $flashMessage = Tools::getFlashMessage('success', 'Slider Image Added successfully');
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function delete(Request $request)
    {
      $slider = Slider::where('unique_key', $request->id)->first();
      \File::delete($slider->image);
      $slider->delete();
    }


}
