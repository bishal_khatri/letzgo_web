<?php

namespace App\Http\Controllers;

use App\ClientMessage;
use App\ContactMessage;
use Illuminate\Http\Request;
use App\Slider;
use App\Service;
use App\Cms;

class FrontController extends Controller
{
    
    public function index()
    {
        $data['slider'] = Slider::all();
        $data['services'] = Service::all();
        $data['video'] = Cms::where('slug','video')->first();
        $data['app_url'] = Cms::where('slug','app_url')->first();
        $data['client_message'] = ClientMessage::all();

        return view('frontend.home',$data);
    }

    public function services()
    {
        $data['services'] = Service::all();

        return view('frontend.services',$data);
    }

    public function contact_message(Request $request)
    {
//        dd($request->all());
        $msg = new ContactMessage();
        $msg->name = $request->name;
        $msg->email = $request->email;
        $msg->subject = $request->subject;
        $msg->message = $request->message;
        $msg->client_ip = \Request::ip();
        $msg->save();
        return 'success';

    }
}
