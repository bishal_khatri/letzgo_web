<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use Hash;
use App\Helper\Tools;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $validate = $this->validateLogin($request);
        if ($validate==false) {
            $responseData = Tools::setResponse('fail','Missing Parameter', '','0');
        }
        else{
            $plainPassword = $request['password'];
            $result = $this->checkUsername($request);
            if (empty($result)) {
                $responseData = Tools::setResponse('fail','Username did not match', '','0');
            }
            else{
                $hashPassword = $result->password;
                $checkPassword = $this->checkPassword($hashPassword,$plainPassword);
                if ($checkPassword==false) {
                    $responseData = Tools::setResponse('fail','Password did not match', '','0');
                }
                else{
                    $data = array();
                    $data[] = [
                        'id'=>$result->id,
                        'airlines_id'=>$result->airlines_id,
                        'logo'=>$result->logo,
                        'airline_name'=>$result->name,
                        'full_name'=>$result->full_name,
                        'profile_image'=>$result->profile_image,
                        'email'=>$result->email,
                        'phone'=>$result->phone,
                        'address'=>$result->address,
                        'city'=>$result->city,
                        'user_name'=>$result->user_name
                    ];
                    $responseData = Tools::setResponse('success','Login successful', $data, '1');
                }

            }

        }

        return response()->json($responseData);
    }

    protected function validateLogin($request)
    {
        if (empty($request['username']) OR empty($request['password'])) {
            $response = false;
        }
        else{
            $response = true;
        }

        return $response;

    }

    protected function checkUsername($request)
    {
        $username = $request['username'];

        $sql = DB::table('users as u')
        		    ->select('u.*','a.name','a.logo')
                ->leftjoin('airlines as a','a.id','u.airlines_id')
                ->where('user_name',$username)
                ->where('active',1)
                ->first();

        return $sql;

    }

    protected function checkPassword($hashPassword,$plainPassword)
    {
        if(Hash::check($plainPassword,$hashPassword)) {
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

}
