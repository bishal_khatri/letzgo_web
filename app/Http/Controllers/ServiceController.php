<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Helper\Tools;

class ServiceController extends Controller
{
    public function index()
    {
      $data['main_menu_name'] = 'Services';
      $data['page_title'] = 'List';
      $data['service'] = Service::all();
      return view('service.index',$data);
    }

    public function store(Request $request)
    {
    	$service = new Service();

    	$service->title = $request->title;
    	$service->icon = $request->icon;
    	$service->body = $request->body;
    	$service->unique_key = str_random(45);
    	$service->save();

    	$flashMessage = Tools::getFlashMessage('success', 'Service "'.$request->title.'" uploaded successfully');
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();

    }

    public function edit($key)
    {
    	$data['main_menu_name'] = 'Services';
		$data['page_title'] = 'Edit';
		$data['service'] = Service::where('unique_key',$key)->first();
		return view('service.edit',$data);
    }

    public function update(Request $request)
    {
    	$service = Service::where('unique_key', $request->key)->first();
    	$service->title = $request->title;
    	$service->icon = $request->icon;
    	$service->body = $request->body;
    	$service->save();

    	$flashMessage = Tools::getFlashMessage('success', 'Service updated successfully');
        \Session::flash('flash_message', $flashMessage);

        return redirect()->route('services.index');
    }

    public function delete(Request $request)
    {
    	$service = Service::where('unique_key', $request->id)->first();
    	$service->delete();

    	$flashMessage = Tools::getFlashMessage('success', 'Service "'.$service->title.'" deleted successfully');
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }


}
