<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Hash;
use Image;
use App\User;
use App\Helper\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class ProfileController extends Controller
{

    use AuthenticatesUsers;

	private $profile;
	public function __construct()
	{
		$this->profile = new User();
	}

    public function index()
    {
		$data['main_menu_name'] = 'Profile';
		$data['page_title'] = 'View';
		$data['user'] = User::find(Auth::user()->id);
		if (Auth::user()->security_key=='superadmin') {
			$data['role'] = 'Super Admin';
		}
		else{
			$data['role'] = $this->profile->roleListing(Auth::user()->id);
		}
		$data['active']=1;
		return view('profile.index', $data);
    }

    public function updateInfo(Request $request)
    {
    	// dd($request->all());
    	$userCheck = $this->profile->checkUser($request['user_name']);
    	$emailCheck = $this->profile->checkEmail($request['email']);
    	if ($userCheck>0) {
    		$flashMessage = Tools::getFlashMessage('danger', 'User Name "'.$request['user_name'].'" already exists');
        	\Session::flash('flash_message', $flashMessage);
    	}
    	elseif($emailCheck>0){
    		$flashMessage = Tools::getFlashMessage('danger', 'User Name "'.$request['email'].'" already exists');
        	\Session::flash('flash_message', $flashMessage);
    	}
    	else{

	        $user = User::where('security_key',$request['security_key'])->first();

	        !empty($request['user_name'] ? $user_name = $request['user_name'] : $user_name = $user->user_name );
	        !empty($request['full_name'] ? $full_name = $request['full_name'] : $full_name = $user->full_name );
	        !empty($request['email'] ? $email = $request['email'] : $email = $user->email );
	        !empty($request['phone'] ? $phone = $request['phone'] : $phone = $user->phone );
	        !empty($request['address'] ? $address = $request['address'] : $address = $user->address );
	        !empty($request['city'] ? $city = $request['city'] : $city = $user->city );

	        $user->user_name = $user_name;
	        $user->full_name = $full_name;
	        $user->email = $email;
	        $user->phone = $phone;
	        $user->address = $address;
	        $user->city = $city;
	        $user->update();

	        $flashMessage = Tools::getFlashMessage('success', 'Updated "'.$user_name.'" successfully');
        	\Session::flash('flash_message', $flashMessage);
    	}

    	return redirect()->route('profile.view');


    }

    public function updateAvatar(Request $request)
    {
    	// dd($request->all());
    	$user = User::where('security_key',$request['key'])->first();
    	if (!empty($user->profile_image)) {
    		$oldImage = $user->profile_image;
    	}
    	// dd($oldImage);
    	$this->validate($request, [
            'image' => 'required',
        ]);
        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $ext = $file->getClientOriginalExtension();
            $image_name = date('YmdHis') . "." . $file->getClientOriginalExtension();
            $destination_path = 'public/uploads/profile/'. $image_name;
            $img = Image::make($file->getRealPath());
            $img->save($destination_path);
        }

        if (isset($image_name)) {
        	$user->profile_image = $image_name;
        	$user->update();
        }
        if (!empty($oldImage)) {
        	$filename = 'public/uploads/profile/'.$oldImage;
        	\File::delete($filename);
        }

        $flashMessage = Tools::getFlashMessage('success', 'Updated "'.$user->user_name.'" successfully');
        \Session::flash('flash_message', $flashMessage);

       	return redirect()->route('profile.view');


    }

    public function updatePassword(Request $request)
    {
    	$this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|confirmed',
        ]);
        $currentPassword = $request['current_password'];

        $user = User::where('security_key',$request['security_key'])->first();

        $check = Hash::check($currentPassword,$user->password);

        if ($check==false) {
        	$flashMessage = Tools::getFlashMessage('danger', 'Current password is not correct');
        	\Session::flash('flash_message', $flashMessage);

        }
        else{
        	$user->password=bcrypt($request['password']);
        	$user->real_password =$request['password'];
        	$user->update();
        	$flashMessage = Tools::getFlashMessage('success', 'Updated "'.$user->user_name.'" successfully');
        	\Session::flash('flash_message', $flashMessage);
        }

       	return redirect()->route('profile.view');
    }

    public function lock()
    {
        if(Auth::check()){
            $data['user'] = User::find(Auth::user()->id);
            \Session::put('locked', true);

            return view('auth.lock',$data);
        }
    }

    public function unLock(Request $request)
    {
         $password = $request->password;

        $this->validate($request, [
            'password' => 'required|string',
        ]);

        if(\Hash::check($password, \Auth::user()->password)){
            $request->session()->forget('locked');
            return redirect('/home');
        }

        return back()->withErrors('Password does not match. Please try again.');

    }

}
