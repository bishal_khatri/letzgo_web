<?php

namespace App\Http\Controllers;

use App\ClientMessage;
use App\ContactMessage;
use App\Cms;
use File;
use App\Helper\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CmsController extends Controller
{
    public function app_url_index()
    {
        $data['main_menu_name'] = 'APP URL';
        $data['page_title'] = 'List';
        $data['app_url'] = Cms::where('slug','app_url')->first();
        return view('app_url.index', $data);
    }

    public function app_url_update(Request $request)
    {
        $app_url = Cms::where('slug','app_url')->first();
        if (is_null($app_url)){
            $app_url = new Cms();
            $app_url->slug = 'app_url';
        }
        if ($request->type=='url'){
            $app_url->url = $request->app_url;
            $app_url->save();
        }
        elseif ($request->type=='text'){
            $app_url->title = $request->app_url_title;
            $app_url->subtitle = $request->app_url_subtitle;
            $app_url->save();
        }

        $flashMessage = Tools::getFlashMessage('success', 'Content changed successfully');
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();

    }

    public function board_index(){
        $data['main_menu_name'] = 'Board';
        $data['page_title'] = 'List';
        $data['board1'] = Cms::where('slug' , 'board1')->first();
        $data['board2'] = Cms::where('slug' , 'board2')->first();
        $data['board3'] = Cms::where('slug' , 'board3')->first();
        return view('board.index' , $data);
    }

    public function board_update(Request $request){
        // dd($request->all());
        if($request->board == 'board1'){    
            $board = Cms::where('slug' , 'board1')->first(); 
            if(is_null($board)){
                $board = new Cms();
                $board->slug = 'board1';
            }
            $board->icon =$request->icon; 
            $board->title = $request->title;
            $board->subtitle = $request->subtitle;
            $board->save();  

            $flashMessage = Tools::getFlashMessage('success', 'Content changed successfully');
            \Session::flash('flash_message', $flashMessage);
            return redirect()->back(); 
        }

        if($request->board == 'board2'){    
            $board = Cms::where('slug' , 'board2')->first(); 
            if(is_null($board)){
                $board = new Cms();
                $board->slug = 'board2';
            }
            $board->icon =$request->icon; 
            $board->title = $request->title;
            $board->subtitle = $request->subtitle;
            $board->save();  

            $flashMessage = Tools::getFlashMessage('success', 'Content changed successfully');
            \Session::flash('flash_message', $flashMessage);
            return redirect()->back(); 
        } 

         if($request->board == 'board3'){    
            $board = Cms::where('slug' , 'board3')->first(); 
            if(is_null($board)){
                $board = new Cms();
                $board->slug = 'board3';
            } 
            $board->icon =$request->icon;
            $board->title = $request->title;
            $board->subtitle = $request->subtitle;
            $board->save();  

            $flashMessage = Tools::getFlashMessage('success', 'Content changed successfully');
            \Session::flash('flash_message', $flashMessage);
            return redirect()->back(); 
        }


    }

    public function video_index()
    {
        $data['main_menu_name'] = 'Video';
        $data['page_title'] = 'List';
        $data['video'] = Cms::where('slug','video')->first();
        return view('video.index', $data);
    }

    public function video_update(Request $request)
    {
        $video = Cms::where('slug','video')->first();
        if (is_null($video)){
            $video = new Cms();
            $video->slug = 'video';
        }
        if (Input::has('image')) {
            $file = Input::file('image');
            $ext = $file->getClientOriginalExtension();
            $file_name = date('YmdHis') . "." . $ext;
            $destination_path = 'public/uploads/video_cover/';
            $full_name = $destination_path . $file_name;
            $request->file('image')->move($destination_path,$file_name);
            $video->cover_image = $full_name;
        }
        $video->url = $request->video_url;
        $video->save();

        $flashMessage = Tools::getFlashMessage('success', 'Content changed successfully');
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();

    }

    public function contact_message_index()
    {
        $data['main_menu_name'] = 'Contact Message';
        $data['page_title'] = 'List';
        $data['message'] = ContactMessage::all();
        return view('contact_message.index' , $data);
    }

    public function contact_message_delete(Request $request)
    {
        
        $msg = ContactMessage::where('id', $request->id)->first();
        $msg->delete();
        $flashMessage = Tools::getFlashMessage('success', 'Message deleted successfully');
        \Session::flash('flash_message', $flashMessage);
    }

    public function client_message_index()
    {
        $data['main_menu_name'] = 'Client Message';
        $data['page_title'] = 'List';
        $data['message'] = ClientMessage::all();
        return view('client_message.index', $data);
    }

    public function client_message_store(Request $request)
    {
        $message = new ClientMessage();

        if (Input::has('image')) {
            $file = Input::file('image');
            $ext = $file->getClientOriginalExtension();
            $file_name = date('YmdHis') . "." . $ext;
            $destination_path = 'public/uploads/client_message/';
            $full_name = $destination_path . $file_name;
            $request->file('image')->move($destination_path,$file_name);
            $message->image = $full_name;
        }
        $message->full_name = $request->name;
        $message->position = $request->position;
        $message->message = $request->message;
        $message->unique_key = str_random(45);
        $message->save();

        $flashMessage = Tools::getFlashMessage('success', 'Message saved successfully');
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();

    }

    public function client_message_delete(Request $request)
    {
        $msg = ClientMessage::where('unique_key', $request->id)->first();
        \File::delete($msg->image);
        $msg->delete();
        $flashMessage = Tools::getFlashMessage('success', 'Message deleted successfully');
        \Session::flash('flash_message', $flashMessage);
    }

}
