<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'user_name', 'password','real_password','register_ip','status','email', 'address','city','phone','security_key',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','security_key',
    ];

    public function roles()
    {
        return $this->belongsToMany(Roles::class, 'role_user');
    }

    public function hasAccess(array $permissions)
    {
            foreach ($this->roles as $role) {
                if ($role->hasAccess($permissions)) {
                    return true;
                }        
            }
            return false;
    }

    // public function airlines()
    // {
    //     return $this->belongsTo('App\Airline','airlines_id');
    // }

    public function roleListing($userId)
    {
        // dd($userId);
        $sql = DB::table('role_user as ru')
                ->select('name')
                ->join('roles as r', 'r.id', '=', 'ru.roles_id')
                ->where('user_id',$userId)
                ->first();
                // dd($sql);
        return $sql;

    }

    public function checkUser($user_name)
    {
        $result = User::where('user_name',$user_name)->count();

        return $result;
    }

    public function checkEmail($email)
    {
        $result = User::where('email',$email)->count();

        return $result;
    }
}
