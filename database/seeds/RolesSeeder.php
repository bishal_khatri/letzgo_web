<?php

use Illuminate\Database\Seeder;
use App\Roles;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $auther = Roles::create([
        	'name' => 'Admin',
        	'slug' => 'admin',
        	'permissions' => json_encode([
                'list-user'=>true,
                'add-user'=>true,
        	]),
        ]);

        $editor = Roles::create([
        	'name' => 'Editor',
        	'slug' => 'editor',
        	'permissions' => json_encode([
        		'list-user'=>true,
        	]),
        ]);
    }
}
