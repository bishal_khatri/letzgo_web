<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
define("ADMIN", "admin/");
define("ADMIN_NAME", "admin.");
// define('USER_IMAGE','public/uploads/profile/avatar.png');
// define("ADMIN", "admin/");
define('INSERT_IMAGES','public/uploads/icons/insert-images.png');
// Admin Routes Start
// FRONTEND ROUTE


Route::get('/',['uses'=>'FrontController@index', 'as'=>'index']);
Route::get('/services',['uses'=>'FrontController@services', 'as'=>'services']);
Route::post('/contact_message',['uses'=>'FrontController@contact_message', 'as'=>'contact_message']);
// FORNTEND ROUTE END

// Auth::routes();

// LOGIN ROUTE START
Route::get(ADMIN.'login', ['uses'=>'Auth\LoginController@showLoginForm','as'=>'login']);
Route::post(ADMIN.'login', ['uses'=>'Auth\LoginController@login']);
Route::get(ADMIN.'logout',['uses'=>'Auth\LoginController@logout', 'as'=> 'logout']);
// LOGIN ROUTE END

// Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Route::get(ADMIN.'home', 'HomeController@index')->name('home');
Route::get(ADMIN.'/', ['uses'=>'HomeController@index', 'as'=>'home']);
Route::get(ADMIN.'/lock', ['uses' => 'ProfileController@lock', 'as' => 'lock']);
Route::post(ADMIN.'/lock', ['uses' => 'ProfileController@unLock', 'as' => 'lock']);
// Module routes start
Route::group(['prefix' =>ADMIN.'module', 'middleware' => ['auth','lock'], 'as' => 'module.'], function () {
	Route::get('/add',['uses' => 'ModuleController@create', 'as' => 'create'])->middleware('can:add-module');
	Route::post('/add',['uses' => 'ModuleController@store'])->middleware('can:add-module');
	Route::get('/list',['uses' => 'ModuleController@listModule','as' => 'list' ])->middleware('can:list-module');
	Route::get('/edit/{key}',['uses' => 'ModuleController@editModuleForm','as' => 'edit' ])->middleware('can:edit-module');
	Route::post('/edit/{key}',['uses' => 'ModuleController@updateModule'])->middleware('can:edit-module');
	Route::post('/delete',['uses' => 'ModuleController@deleteModule','as' => 'delete' ])->middleware('can:delete-module');
});
// Module route end

// Menu route start
Route::group(['prefix' =>ADMIN.'menu', 'middleware' => ['auth','lock'], 'as' => 'menu.'], function () {
	Route::get('/view/{id}',['uses' => 'ModuleController@viewMenu', 'as' => 'view']);
	Route::get('/add/{id}/{name}',['uses' => 'ModuleController@createMenu', 'as' => 'create']);
	Route::post('/store',['uses' => 'ModuleController@storeMenu','as' => 'store']);
});
// Menu route end

// users route start
Route::group(['prefix' =>ADMIN.'user', 'middleware' => ['auth','lock'], 'as' => 'user.'], function () {
	Route::get('/register',['uses'=>'UserController@showRegistrationForm', 'as'=> 'register'])->middleware('can:add-user');
	Route::post('/register',['uses'=>'UserController@store'])->middleware('can:add-user');
	Route::get('/list',['uses'=>'UserController@listUser', 'as' => 'list'])->middleware('can:list-user');
	Route::get('/edit/{key}',['uses'=>'UserController@getEdit', 'as' => 'edit'])->middleware('can:edit-user');
	Route::post('/update/{key}',['uses'=>'UserController@update', 'as'=>'update'])->middleware('can:edit-user');
	Route::post('/delete',['uses'=>'UserController@delete', 'as'=>'delete'])->middleware('can:delete-user');
	Route::post('/suspend',['uses'=>'UserController@suspend', 'as'=>'suspend'])->middleware('can:suspend-user');
	Route::post('/restore',['uses'=>'UserController@restore', 'as'=>'restore'])->middleware('can:suspend-user');
});
// users route end

// roles route start
Route::group(['prefix' =>ADMIN.'role', 'middleware' => ['auth','lock'], 'as' => 'role.'], function () {
	Route::any('/list', ['uses' => 'RolesController@index', 'as' => 'list'])->middleware('can:list-role');
	Route::any('/add', ['uses' => 'RolesController@showRolesFrom', 'as' => 'add'])->middleware('can:add-role');
	Route::post('/add', ['uses' => 'RolesController@rolesStore', 'as' => 'add'])->middleware('can:add-role');
	Route::get('/edit/{id}', ['uses' => 'RolesController@showRolesEditFrom', 'as' => 'edit'])->middleware('can:edit-role');
	Route::post('/edit/{id}', ['uses' => 'RolesController@update'])->middleware('can:edit-role');
	Route::post('/delete', ['uses' => 'RolesController@delete', 'as' => 'delete'])->middleware('can:delete-role');
});
	// roles route end

// Profile route start
Route::group(['prefix' =>ADMIN.'profile', 'middleware' => ['auth','lock'], 'as' => 'profile.'], function () {
	Route::any('/view', ['uses' => 'ProfileController@index', 'as' => 'view']);
	Route::post('/info', ['uses' => 'ProfileController@updateInfo', 'as' => 'info']);
	Route::post('/avatar', ['uses' => 'ProfileController@updateAvatar', 'as' => 'avatar']);
	Route::post('/password', ['uses' => 'ProfileController@updatePassword', 'as' => 'password']);
});
// Profile route end

// CMS ROUTE START
Route::group(['prefix' =>ADMIN.'slider', 'middleware' => ['auth','lock'], 'as' => 'slider.'], function () {
	Route::any('/list', ['uses' => 'SliderController@index', 'as' => 'index'])->middleware('can:list-slider');
	Route::post('/upload', ['uses' => 'SliderController@upload', 'as' => 'upload'])->middleware('can:list-slider');
	Route::post('/delete', ['uses' => 'SliderController@delete', 'as' => 'delete'])->middleware('can:delete-slider');
});

Route::group(['prefix' =>ADMIN.'services', 'middleware' => ['auth','lock'], 'as' => 'services.'], function () {
	Route::any('/list', ['uses' => 'ServiceController@index', 'as' => 'index'])->middleware('can:list-services');
	Route::post('/store', ['uses' => 'ServiceController@store', 'as' => 'store'])->middleware('can:list-services');
	Route::get('/edit/{key}', ['uses' => 'ServiceController@edit', 'as' => 'edit'])->middleware('can:list-services');
	Route::post('/update', ['uses' => 'ServiceController@update', 'as' => 'update'])->middleware('can:list-services');
	Route::post('/delete', ['uses' => 'ServiceController@delete', 'as' => 'delete'])->middleware('can:delete-services');
});

Route::group(['prefix' =>ADMIN.'video', 'middleware' => ['auth','lock'], 'as' => 'video.'], function () {
	Route::any('/list', ['uses' => 'CmsController@video_index', 'as' => 'index']);
	Route::post('/update', ['uses' => 'CmsController@video_update', 'as' => 'update']);
});

Route::group(['prefix' =>ADMIN.'board', 'middleware' => ['auth','lock'], 'as' => 'board.'], function () {
	Route::any('/list', ['uses' => 'CmsController@board_index', 'as' => 'index']);
	Route::post('/update', ['uses' => 'CmsController@board_update', 'as' => 'update']);
});

Route::group(['prefix' =>ADMIN.'client_message', 'middleware' => ['auth','lock'], 'as' => 'client_message.'], function () {
	Route::any('/list', ['uses' => 'CmsController@client_message_index', 'as' => 'index']);
	Route::post('/store', ['uses' => 'CmsController@client_message_store', 'as' => 'store']);
	Route::post('/delete', ['uses' => 'CmsController@client_message_delete', 'as' => 'delete']);
});

Route::group(['prefix' =>ADMIN.'contact_message', 'middleware' => ['auth','lock'], 'as' => 'contact_message.'], function () {
	Route::any('/list', ['uses' => 'CmsController@contact_message_index', 'as' => 'index']);
	Route::post('/delete', ['uses' => 'CmsController@contact_message_delete', 'as' => 'delete']);
});

Route::group(['prefix' =>ADMIN.'app_url', 'middleware' => ['auth','lock'], 'as' => 'app_url.'], function () {
    Route::any('/list', ['uses' => 'CmsController@app_url_index', 'as' => 'index']);
    Route::post('/update', ['uses' => 'CmsController@app_url_update', 'as' => 'update']);
});
// CMS ROUTE END