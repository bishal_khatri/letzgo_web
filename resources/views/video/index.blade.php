@extends('layouts.app')

@section('css')
<link href="{{ url('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('public/frontend/css/style.css')}}"/>


@endsection

@section('title') Slider Listing @endsection

@section('add-button')
@can('add-slider')
@endcan
@endsection

@section('content')
<div class="portlet-body">
  <div class="row">
    <div class="col-md-8">
          <table class="table table-striped">
              <div class="intro-video">
                  <div class="row">
                      <div class="col-md-8 col-md-offset-2">
                          <img src="{{ asset($video->cover_image)}}" alt="">
                          <a href="{{ $video->url }}" class="video-popup">
                              <i class="fa fa-play"></i>
                          </a>
                      </div>
                  </div>
              </div>
          </table>
    </div>
    <div class="col-md-4">
      <form role="form" action="{{ route('video.update') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9">Image</label>
              <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                      @if (!empty($video->cover_image))
                          <img src="{{ asset($video->cover_image) }}" alt=""/>
                      @else
                          <img src="{{ url(INSERT_IMAGES) }}" alt=""/>
                      @endif
                  </div>
                  <div class="fileinput-preview fileinput-exists thumbnail"
                       style="max-width: 200px; max-height: 200px;">
                  </div>
                  <div>
                      <span class="btn default btn-file" style="width: 200px;">
                          <span class="fileinput-new"> Select Cover Image </span>
                          <span class="fileinput-exists"> Change </span>
                          <input type="file" id="img" name="image" data-allowed-file-extensions='["png", "jpg"]'/>
                      </span>
                      <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
              </div>

              <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                  <label class="control-label ">Video URL</label>
                  <textarea class="form-control placeholder-no-fix" type="text" placeholder="Video URL" name="video_url" rows="5"/>{{ $video->url or '' }}</textarea>
              </div>

              <div class="form-actions noborder">
                  <div class="row">
                      <div class="col-md-10">
                          <button type="submit" class="btn green">Update</button>
                           <button class="btn default" type="reset" >Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
      </form>
    </div>
  </div>
</div>

@include('slider.model')
@endsection

@section('script')

<script src="{{url('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

@endsection
