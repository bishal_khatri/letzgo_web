<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>{{ env('APP_NAME','Admin') }} | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="Bishal Khatri"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('public/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/login-soft.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="{{ asset('public/global/css/components-md.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/css/plugins-md.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/layout.css')}}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{ asset('public/global/default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/global/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<style media="screen">
  .login .content{
    border: 1px solid silver;

  }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="#">
    <img src="{{ asset('public/global/logo.png')}}" alt="" height="80" width="80" />
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('login') }}" method="post" autocomplete="off">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="on" id="">
        <h3 class="form-title">Login to your account</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
            Enter any username and password. </span>
        </div>
        <div class="form-group {{ $errors->has('user_name') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" required="" value="{{ old('user_name') }}" />
            </div>
            @if ($errors->has('user_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="passwordInput" placeholder="Password" name="password" required="" />
            </div>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-actions" style="padding-bottom:0px; padding-top:0px; margin-top:-10px;">
            <label class="checkbox">
            <input type="checkbox" onclick="showPassword()">Show Password
        </div>
        <div class="form-actions">
            <label class="checkbox">
            <input type="checkbox" name="remember" value="1" {{ old('remember') ? 'checked' : '' }} /> Remember me </label>
            <button type="submit" class="btn blue pull-right">
            Login <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <!-- END LOGIN FORM -->


</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
     2018 &copy; Technology Sales: {{ env('APP_NAME','Admin') }}
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{ asset('public/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('public/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/plugins/backstretch/jquery.backstretch.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('public/global/plugins/select2/select2.min.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('public/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/global/scripts/login-soft.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "{{ asset('public/global/img/bg/1.jpg')}}",
        "{{ asset('public/global/img/bg/2.jpg')}}",
        "{{ asset('public/global/img/bg/3.jpg')}}",
        "{{ asset('public/global/img/bg/4.jpg')}}"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
});

function showPassword() {
   var x = document.getElementById("passwordInput");
   if (x.type === "password") {
       x.type = "text";
   } else {
       x.type = "password";
   }
}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
