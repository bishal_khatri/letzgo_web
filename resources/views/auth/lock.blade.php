<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>FIDS | Lock Screen</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('public/assets/admin/pages/css/lock2.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{ asset('public/assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/admin/layout/css/themes/darkblue.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{ asset('public/assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<style type="text/css">
	.profile-img{
		max-height: 200px;
		max-width: 200px;
	}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<div class="page-lock">
	<div class="page-logo">
		<a class="brand" href="index.html">
		<!-- <img src="{{ asset('public/uploads/icons/tech.gif')}}" alt="logo"/> -->
		</a>
	</div>
	<div class="page-body">
		@if(empty($user->profile_image))
        	<img src="{{ url('public/uploads/profile/avatar.png') }}" class="img-responsive" alt=""> 
        @else
        	<img class="profile-img" src="{{ asset("public/uploads/profile/$user->profile_image") }}" class="img-responsive" alt="">
        @endif
		<div class="page-lock-info">
			<h1>{{$user->full_name}}</h1>
			<span class="email">{{ $user->email }}</span>
			<span class="locked" style="color: red;">
			Locked </span>
			<form class="form-inline" action="{{ route('lock') }}" method="post">
				{{ csrf_field() }}
				<div class="input-group input-medium {{ $errors ? ' has-error' : '' }}">
					<input type="hidden" name="user_name" value="{{ $user->user_name }}">
					<input type="password" class="form-control" placeholder="Password" name="password">
					<span class="input-group-btn">
					<button type="submit" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></button>
					</span>
				</div>
				<!-- /input-group -->
				<div class="relogin">
					<a href="{{ route('logout') }}">
					Not {{ $user->full_name }} ? </a>
				</div>
			</form>
		</div>
	</div>
	<div class="page-footer-custom">
		 2018 &copy; Technology Sales: Flight Information System.
	</div>
</div>
<script src="{{ asset('public/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('public/assets/global/plugins/backstretch/jquery.backstretch.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{ asset('public/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/assets/admin/pages/scripts/lock.js')}}"></script>
<script>
jQuery(document).ready(function() {
    Metronic.init(); // init metronic core components
Layout.init(); // init current layout
    Lock.init();
    Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
