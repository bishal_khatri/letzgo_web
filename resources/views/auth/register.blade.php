@extends('layouts.app')

@section('css')
 <link href="{{ url('public/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('title') Register @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
<div class="portlet-body">
    <form class="register-form" action="{{ route('user.register') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <p style="color: #FF7D01;"> Enter your personal details below: </p>
        <div class="form-group">
            <label class="control-label "></label>
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <?php $myfile = (isset($publicationsSel->image)) ? $publicationsSel->image : ''?>
                    @if (File::exists($myfile))
                        <img src="{!! url($myfile) !!}" alt=""/>
                        <input type="hidden" name="image" value="{{ $myfile or 'default' }}">
                    @else
                        <img src="{{ url('public/uploads/icons/insert-images.png') }}" alt=""/>
                    @endif
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"
                     style="max-width: 200px; max-height: 150px;">
                </div>
                <div>
                    <span class="btn default btn-file" style="width: 200px;">
                        <span class="fileinput-new"> Select Profile Picture </span>
                        <span class="fileinput-exists"> Change </span>
                        <input type="file" id="img" name="image" data-allowed-file-extensions='["png", "jpg"]'/>
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                </div>
                <div style="max-width: 500px; padding-top: 10px;"> Approprate Size :
                    <span style="color: #FF7D01;">W:310px, H:255px </span>
                </div>
            </div>
        </div>
        <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }}">
            <label class="control-label ">Full Name</label>
            <div class="input-icon">
                <i class="fa fa-font" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="full_name" value="{{ old('full_name') }}" />
            </div>
             @if ($errors->has('full_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('full_name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label ">Email</label>
            <div class="input-icon">
                <i class="fa fa-envelope" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email" value="{{ old('email') }}"/>
            </div>
        </div>
        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
            <label class="control-label ">Phone</label>
            <div class="input-icon">
                <i class="fa fa-phone" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Phone" name="phone" value="{{ old('phone') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label ">Address</label>
            <div class="input-icon">
                <i class="fa fa-check" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="address" value="{{ old('address') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label ">City/Town</label>
            <div class="input-icon">
                <i class="fa fa-location-arrow" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="city" value="{{ old('city') }}"/>
            </div>
        </div>
        <p style="color: #FF7D01;"> Enter your account details below: </p>
        <div class="form-group {{ $errors->has('user_name') ? ' has-error' : '' }}">
            <label class="control-label ">Username</label>
            <div class="input-icon">
                <i class="fa fa-user" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" value="{{ old('user_name') }}" />
            </div>
             @if ($errors->has('user_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('roles') ? ' has-error' : '' }}">
            <label class="control-label ">Roles</label>
            <div class="input-icon">
                <i class="fa fa-lock" id="icon-defult"></i>
                <select name="roles" id="" class="form-control">
                @if(isset($roles))
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                @endif
                </select>
            </div>
             @if ($errors->has('roles'))
                <span class="help-block">
                    <strong>{{ $errors->first('roles') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label ">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock" id="icon-defult"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password"/>
            </div>
             @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label class="control-label ">Re-type Your Password</label>
            <div class="controls">
                <div class="input-icon">
                    <i class="fa fa-check" id="icon-defult"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation"/>
                </div>
                 @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
            </div>
        </div>
        <div class="form-group">
            <label>
            <input type="checkbox" name="tnc"/> I agree to the <a href="javascript:;">
            Terms of Service </a>
            and <a href="javascript:;">
            Privacy Policy </a>
            </label>
            <div id="register_tnc_error">
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-8">
                   <button type="submit" class="btn green">Submit</button>
                    <a class="btn default" href="{{ URL::previous() }}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script src="{{url('public/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection
