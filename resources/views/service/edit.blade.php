@extends('layouts.app')

@section('title') Update @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
<div class="portlet-body">
    <form role="form" action="{{ route('services.update') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="key" value="{{ $service->unique_key }}">
          <div class="form-group">
              <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                  <label class="control-label ">Icon</label>
                  <input class="form-control placeholder-no-fix" type="text" placeholder="Icon" name="icon" value="{{ $service->icon }}"/>
              </div>

              <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                  <label class="control-label ">Title</label>
                  <input class="form-control placeholder-no-fix" type="text" placeholder="Title" name="title" value="{{ $service->title }}"/>
              </div>

              <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
                  <label class="control-label ">Body</label>
                  <textarea class="form-control placeholder-no-fix" rows="8" type="text" placeholder="Body" name="body" />{{ $service->body }}</textarea>
              </div>

              <div class="form-actions noborder">
                  <div class="row">
                      <div class="col-md-10">
                          <button type="submit" class="btn green">Update</button>
                           <button class="btn default" type="reset" >Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
      </form>
</div>

@endsection
