@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('public/frontend/css/flaticon.css')}}"/>
<link href="{{ asset('public\assets\lightbox\src\css\lightbox.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{ url('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<style>
.terminated{
    background-color: rgba(189, 195, 91, 0.35) !important;
    color: #000 !important;
}
img {
    max-width: 200px;
    padding-bottom: 5px;
    padding-left: 2px;
}

</style>
@endsection

@section('title') Services Listing @endsection

@section('add-button')
@can('add-slider')
@endcan
@endsection

@section('content')
<div class="portlet-body">
  <div class="row">
    <div class="col-md-8">
           
      <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th> Icon</th>
                <th> Title</th>
                <th> Body</th>
                <th style="width: 40px;"> Action</th>
            </tr>
        </thead>
        <tbody>
             @if (isset($service))
              @foreach ($service as $val)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><i class="{{ $val->icon }}"></i></td>
                <td>{{ $val->title }}</td>
                <td style="text-align: center;">
                    <a href="#view" data-toggle="modal" data-ids="{{ $val->title }}" data-user="{{ $val->body }}">View</a>
                </td>
                <td style="text-align: center; width: 100px;">
                  <a href="{{ route('services.edit',$val->unique_key) }}" style="padding-right: 10px; ">
                    <i class="fa fa-pencil icon" id="icon-defult" ></i></a>
                    <a href="#confirmDelete" data-ids="{{ $val->unique_key }}"
                                             data-user="" data-rel="delete"
                                             data-toggle="modal"
                                             data-title="Delete"
                                             data-message='Are you sure you want to terminate this row ?' style="padding-left: 10px; ">
                                <i class="fa fa-trash" id="icon-terminate" ></i></a>
                </td>
            </tr>
              @endforeach
            @endif
      </table>
    </div>
    <div class="col-md-4">
      <form role="form" action="{{ route('services.store') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
              <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                  <label class="control-label ">Icon</label>
                  <input class="form-control placeholder-no-fix" type="text" placeholder="Icon" name="icon" value="{{ old('icon') }}"/>
              </div>

              <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                  <label class="control-label ">Title</label>
                  <input class="form-control placeholder-no-fix" type="text" placeholder="Title" name="title" value="{{ old('title') }}"/>
              </div>

              <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
                  <label class="control-label ">Body</label>
                  <textarea class="form-control placeholder-no-fix" rows="8" type="text" placeholder="Body" name="body" value="{{ old('body') }}"/></textarea>
              </div>

              <div class="form-actions noborder">
                  <div class="row">
                      <div class="col-md-10">
                          <button type="submit" class="btn green">Submit</button>
                           <button class="btn default" type="reset" >Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
      </form>
    </div>
  </div>
</div>

@include('service.model')
@endsection

@section('script')

<script src="{{ asset('public\assets\lightbox\src\js\lightbox.js')}}" type="text/javascript"></script>
<script src="{{url('public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
   // data table script
var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#data-table');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [
                {"orderable": true },
                {"orderable": false },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": false }
            ],
            "lengthMenu": [
                [-1, 5, 15, 20],
                ["All", 5, 15, 20] // change per page values here
            ],
            // set the initial value
            "pageLength": -1,
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': true,
                'targets': [4]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });
    }
    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }
    };

}();
jQuery(document).ready(function() {
   TableManaged.init();
});
// data table script end
$('#view').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#title").html(ids);
    $("#body").html(user);
});
// AJAX DELETE
$('#confirmDelete').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#hidden_id").val(ids);
    $(".hidden_title").html(' "' + user + '" ');
});

$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#hidden_id").val();
    // console.log(id);
    $.ajax({
        type: "POST",
        url: "{{ route('services.delete') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            // console.log(msg);
            $("#confirmDelete").modal("hide");
            window.location.reload();
        }
    });
});
// AJAX DELETE END
</script>

@endsection
