
<!--====== Javascripts & Jquery ======-->
<script src="{{ asset('public/frontend/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{ asset('public/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('public/frontend/js/magnific-popup.min.js')}}"></script>
<script src="{{ asset('public/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('public/frontend/js/circle-progress.min.js')}}"></script>
<script src="{{ asset('public/frontend/js/main.js')}}"></script>