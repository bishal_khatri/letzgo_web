<!-- Header section -->
<header class="header-section">
    <div class="logo">
        <img src="{{ asset('public/frontend/img/logo.png')}}" alt=""><!-- Logo -->
    </div>
    <!-- Navigation -->
    <div class="responsive"><i class="fa fa-bars"></i></div>
    <nav>
        <ul class="menu-list">
            <li class="@if(Request::is('/')) active @endif"><a href="{{ route('index') }}">Home</a></li>
            <li class="@if(Request::is('services')) active @endif"><a href="{{ route('services') }}">Services</a></li>
            <li class="@if(Request::is('blog')) active @endif"><a href="blog.html">Blog</a></li>
            <li class="@if(Request::is('contact')) active @endif"><a href="contact.html">Contact</a></li>
            <li class="@if(Request::is('elements')) active @endif"><a href="elements.html">Elements</a></li>
        </ul>
    </nav>
</header>
<!-- Header section end -->