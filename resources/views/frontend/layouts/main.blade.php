<!DOCTYPE html>
<html lang="en">
    @include('frontend.layouts.partials._head')
<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader">
        <img src="{{ asset('public/frontend/img/logo.png')}}" alt="">
        <h2>Loading.....</h2>
    </div>
</div>

    @include('frontend.layouts.partials._nav')

    @yield('content')

    @include('frontend.layouts.partials._footer')


    @include('frontend.layouts.partials._scripts')

<script>
    $('#con_form').on('submit', function(e) {
        e.preventDefault();

        var name = this['name'].value;
        var email = this['email'].value;
        var subject = this['subject'].value;
        var message = this['message'].value;

        var dataString = '&name=' + name + '&email=' + email + '&subject=' + subject + '&message=' + message;
        // console.log(data);
        $.ajax({
            type: "POST",
            url: "{{ route('contact_message') }}",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: dataString,
            success: function (msg) {
                // console.log(msg);
                // $("#confirmDelete").modal("hide");
                // window.location.reload();
            }
        })
    });
</script>
</body>
</html>
