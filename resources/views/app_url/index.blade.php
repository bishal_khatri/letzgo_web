@extends('layouts.app')

@section('css')


@endsection

@section('title') Slider Listing @endsection

@section('add-button')
@can('add-slider')
@endcan
@endsection

@section('content')
<div class="portlet-body">
  <div class="row">
      <div class="col-md-6">
          @if (!empty($app_url->url))
              <a href="{{ $app_url->url }}" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-download" style="padding-right: 10px;"></i>Download</a>
          @else
              <p style="text-align: center;">No download link found.</p>
          @endif
      </div>
      <div class="col-md-6">
          <form role="form" action="{{ route('app_url.update') }}" method="post" enctype="multipart/form-data">
              <p style="color: #FF7D01;"> Enter application URL below: </p>
              {{ csrf_field() }}
              <input type="hidden" name="type" value="url" id="">
              <div class="form-group">
                  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                      <label class="control-label ">Application URL</label>
                      <textarea class="form-control placeholder-no-fix" type="text" placeholder="Application URL" name="app_url" rows="5"/>{{ $app_url->url or '' }}</textarea>
                  </div>
                  <button type="submit" class="btn green">Update</button>
                  <button class="btn default" type="reset" >Cancel</button>
              </div>
          </form>
          <hr>
          <form role="form" action="{{ route('app_url.update') }}" method="post" enctype="multipart/form-data">
              <p style="color: #FF7D01;"> Enter title and subtitle below: </p>
              {{ csrf_field() }}
              <input type="hidden" name="type" value="text" id="">
              <div class="form-group">
                  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                      <label class="control-label ">Title</label>
                      <input class="form-control placeholder-no-fix" type="text" placeholder="Title" name="app_url_title" value="{{ $app_url->title or '' }}">
                  </div>

                  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                      <label class="control-label ">Sub-title</label>
                      <textarea class="form-control placeholder-no-fix" type="text" placeholder="Sub-title" name="app_url_subtitle" rows="5"/>{{ $app_url->subtitle or '' }}</textarea>
                  </div>

                  <div class="form-actions noborder">
                      <div class="row">
                          <div class="col-md-10">
                              <button type="submit" class="btn green">Update</button>
                              <button class="btn default" type="reset" >Cancel</button>
                          </div>
                      </div>
                  </div>
              </div>
          </form>
      </div>
    </div>
</div>

@endsection

@section('script')


@endsection
