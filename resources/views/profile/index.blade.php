@extends('layouts.app')

@section('css')
 <link href="{{ url('public/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ url('public/pages/css/profile.css')}}" rel="stylesheet" type="text/css" />
<style>
.terminated{
    background-color: rgba(189, 195, 91, 0.35) !important;
    color: #000 !important;
}
.profile-sidebar-portlet {
    padding: 30px 0 0 0 !important;
    border: 2px solid #f5f5f5;
}
</style>
@endsection

@section('title') Profile @endsection

@section('add-button')
@can('add-user')
<a href="{{ route('user.register') }}" class="btn btn-outline green"> <i class="icon-plus"></i> Add new </a>
@endcan
@endsection

@section('content')
{{-- {{dd($user)}} --}}
<div class="portlet-body">
	<div class="row">
	    <div class="col-md-12">
	        <!-- BEGIN PROFILE SIDEBAR -->
	        <div class="profile-sidebar">
	            <!-- PORTLET MAIN -->
	            <div class="portlet light profile-sidebar-portlet ">
	                <!-- SIDEBAR USERPIC -->
	                <div class="profile-userpic">
	                	@if(empty($user->profile_image))
	                    	<img src="{{ url('public/uploads/profile/avatar.png') }}" class="img-responsive" alt="">
	                    @else
	                    	<img src="{{ asset("public/uploads/profile/$user->profile_image") }}" class="img-responsive" alt="">
	                    @endif
	                </div>
	                <!-- END SIDEBAR USERPIC -->
	                <!-- SIDEBAR USER TITLE -->
	                <div class="profile-usertitle">
	                    <div class="profile-usertitle-name"> {{ $user->full_name }} </div>
	                    @if(Auth::user()->security_key=='superadmin')
	                    	<div class="profile-usertitle-job"> {{$role}} </div>
	                    @else
	                    	<div class="profile-usertitle-job"> {{$role->name}} </div>
	                    @endif
	                </div>
	                <!-- END SIDEBAR USER TITLE -->
	                <!-- SIDEBAR MENU -->
	                <div class="profile-usermenu">
	                    <ul class="nav">
	                        {{-- <li>
	                            <a href="page_user_profile_1.html">
	                                <i class="icon-home"></i> Overview </a>
	                        </li> --}}
	                        <li class="active">
	                            <a href="javascript:;">
	                                <i class="icon-settings"></i> Account Settings </a>
	                        </li>
	                        {{-- <li>
	                            <a href="page_user_profile_1_help.html">
	                                <i class="icon-info"></i> Help </a>
	                        </li> --}}
	                    </ul>
	                </div>
	                <!-- END MENU -->
	            </div>
	            <!-- END PORTLET MAIN -->
	            <!-- PORTLET MAIN -->
	            <div class="portlet light ">
	                <!-- STAT -->
	                <div class="row list-separated profile-stat">
	                    <div class="col-md-12 col-sm-12 col-xs-12">
	                        <div class="uppercase profile-stat-title"> apple </div>
	                        <div class="uppercase profile-stat-text"> ball </div>
	                    </div>
	                  {{--   <div class="col-md-4 col-sm-4 col-xs-6">
	                        <div class="uppercase profile-stat-title"> 51 </div>
	                        <div class="uppercase profile-stat-text"> Flight Assigned </div>
	                    </div>
	                    <div class="col-md-4 col-sm-4 col-xs-6">
	                        <div class="uppercase profile-stat-title"> 61 </div>
	                        <div class="uppercase profile-stat-text"> Uploads </div>
	                    </div> --}}
	                </div>
	                <!-- END STAT -->
	            </div>
	            <!-- END PORTLET MAIN -->
	        </div>
	        <!-- END BEGIN PROFILE SIDEBAR -->
	        <!-- BEGIN PROFILE CONTENT -->
	        <div class="profile-content">
	            <div class="row">
	                <div class="col-md-12">
	                    <div class="portlet light ">
	                        <div class="portlet-title tabbable-line">
	                            <div class="caption caption-md">
	                                <i class="icon-globe theme-font hide"></i>
	                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
	                            </div>
	                            <ul class="nav nav-tabs">
	                                <li class="active">
	                                    <a href="#tab_1_1" data-toggle="tab" aria-expanded="false">Personal Info</a>
	                                </li>
	                                <li class="">
	                                    <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">Change Avatar</a>
	                                </li>
	                                <li class="">
	                                    <a href="#tab_1_3" data-toggle="tab" aria-expanded="true">Change Password</a>
	                                </li>
	                                {{-- <li class="">
	                                    <a href="#tab_1_4" data-toggle="tab" aria-expanded="false">Privacy Settings</a>
	                                </li> --}}
	                            </ul>
	                        </div>
	                        {{-- {{dd($active)}} --}}
	                        <div class="portlet-body">
	                            <div class="tab-content">
	                                <!-- PERSONAL INFO TAB -->
	                                <div class="tab-pane active" id="tab_1_1">
	                                    <form role="form" action="{{ route("profile.info") }}" method="post" enctype="multipart/form-data">
	                                    	{{ csrf_field() }}
	                                    	<input type="hidden" name="security_key" value="{{$user->security_key}}">
	                                    	<div class="form-group">
	                                            <label class="control-label">User Name</label>
	                                            <input type="text" name="user_name" placeholder="{{ $user->user_name or '' }}" class="form-control" >
	                                        </div>
	                                        <div class="form-group">
	                                            <label class="control-label">Full Name</label>
	                                            <input type="text" name="full_name" placeholder="{{ $user->full_name or '' }}" class="form-control" >
	                                        </div>
	                                        <div class="form-group">
	                                            <label class="control-label">Email</label>
	                                            <input type="text" name="email" placeholder="{{ $user->email or '' }}" class="form-control" >
	                                        </div>
	                                        <div class="form-group">
	                                            <label class="control-label">Mobile Number</label>
	                                            <input type="text" name="phone" placeholder="{{ $user->phone or '' }}" class="form-control">
	                                        </div>
	                                        <div class="form-group">
	                                            <label class="control-label">Address</label>
	                                            <input type="text" name="address" placeholder="{{ $user->address or '' }}" class="form-control"> </div>
	                                        <div class="form-group">
	                                            <label class="control-label">City</label>
	                                            <input type="text" name="city" placeholder="{{ $user->city or '' }}" class="form-control">
	                                        </div>
	                                        <div class="margiv-top-10">
	                                            <button type="submit" class="btn green"> Save Changes </button>
	                                            <button type="reset" class="btn default"> Cancel </button>
	                                        </div>
	                                    </form>
	                                </div>
	                                <!-- END PERSONAL INFO TAB -->
	                                <!-- CHANGE AVATAR TAB -->
	                                <div class="tab-pane" id="tab_1_2">
	                                    <p> Upload your profile image.</p>
	                                    <form action="{{ route("profile.avatar") }}" method="post" enctype="multipart/form-data" role="form">
	                                    	{{ csrf_field() }}
	                                        <div class="form-group">
	                                            <div class="fileinput fileinput-new" data-provides="fileinput">
	                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
	                                                    <img src="{{ url('public/uploads/icons/insert-images.png') }}" alt=""> </div>
	                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
	                                                <div>
	                                                    <span class="btn default btn-file">
	                                                        <span class="fileinput-new"> Select image </span>
	                                                        <span class="fileinput-exists"> Change </span>
	                                                        <input type="hidden" value="{{ $user->security_key }}" name="key"><input type="file" name="image"> </span>
	                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
	                                                </div>
	                                            </div>
	                                            {{-- <div class="clearfix margin-top-10">
	                                                <span class="label label-danger">NOTE! </span>
	                                                <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
	                                            </div> --}}
	                                        </div>
	                                        <div class="margin-top-10">
	                                            <button type="submit" class="btn green"> Upload </button>
	                                            <button type="reset" class="btn default"> Cancel </button>
	                                        </div>
	                                    </form>
	                                </div>
	                                <!-- END CHANGE AVATAR TAB -->
	                                <!-- CHANGE PASSWORD TAB -->
	                                <div class="tab-pane" id="tab_1_3">
	                                    <form action="{{ route("profile.password") }}" method="post">
	                                    	{{ csrf_field() }}
	                                    	<input type="hidden" name="security_key" value="{{ $user->security_key }}">
	                                        <div class="form-group">
	                                            <label class="control-label">Current Password</label>
	                                            <input type="password" name="current_password" class="form-control" required=""> </div>
	                                        <div class="form-group">
	                                            <label class="control-label">New Password</label>
	                                            <input type="password" name="password" class="form-control" required=""> </div>
	                                        <div class="form-group">
	                                            <label class="control-label" required="">Re-type New Password</label>
	                                            <input type="password" name="password_confirmation" class="form-control"> </div>
	                                        <div class="margin-top-10">
	                                            <button type="submit"class="btn green"> Change Password </button>
	                                            <button type="reset"class="btn default"> Cancel </button>
	                                        </div>
	                                    </form>
	                                </div>
	                                <!-- END CHANGE PASSWORD TAB -->
	                                <!-- PRIVACY SETTINGS TAB -->
	                                <div class="tab-pane" id="tab_1_4">
	                                    <form action="#">
	                                        <table class="table table-light table-hover">
	                                            <tbody><tr>
	                                                <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
	                                                <td>
	                                                    <div class="mt-radio-inline">
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios1" value="option1"> Yes
	                                                            <span></span>
	                                                        </label>
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios1" value="option2" checked=""> No
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
	                                                <td>
	                                                    <div class="mt-radio-inline">
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios11" value="option1"> Yes
	                                                            <span></span>
	                                                        </label>
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios11" value="option2" checked=""> No
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
	                                                <td>
	                                                    <div class="mt-radio-inline">
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios21" value="option1"> Yes
	                                                            <span></span>
	                                                        </label>
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios21" value="option2" checked=""> No
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
	                                                <td>
	                                                    <div class="mt-radio-inline">
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios31" value="option1"> Yes
	                                                            <span></span>
	                                                        </label>
	                                                        <label class="mt-radio">
	                                                            <input type="radio" name="optionsRadios31" value="option2" checked=""> No
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
	                                                </td>
	                                            </tr>
	                                        </tbody></table>
	                                        <!--end profile-settings-->
	                                        <div class="margin-top-10">
	                                            <a href="javascript:;" class="btn red"> Save Changes </a>
	                                            <a href="javascript:;" class="btn default"> Cancel </a>
	                                        </div>
	                                    </form>
	                                </div>
	                                <!-- END PRIVACY SETTINGS TAB -->
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- END PROFILE CONTENT -->
	    </div>
	</div>
</div>


@endsection

@section('script')
<script src="{{url('public/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection
