@extends('layouts.app')
@section('title') Home @endsection
@section('css')
<style>
.portlet > .portlet-title > .tools > a {
    margin-top: -10px;
    display: inline-block;
    height: 35px;
    margin-left: 5px;
    opacity: 1;
    filter: alpha(opacity=100);
    background-color: transparent !important;
}
a{
    display: none;
}
</style>
@endsection

@section('content')
	<!-- BEGIN DASHBOARD STATS -->
	<div class="row cover">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light blue-soft" href="{{ route('services.index') }}">
			<div class="visual">
				<i class="fa fa-globe"></i>
			</div>
			<div class="details">
				<div class="number">Services</div>
				<div class="desc">  </div>
			</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light red-soft" href="{{ route('video.index')}}">
			<div class="visual">
				<i class="icon-control-play"></i>
			</div>
			<div class="details">
				<div class="number">
					Video
				</div>
				<div class="desc">URL</div>
			</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light green-soft" href="{{ route('client_message.index') }}">
			<div class="visual">
				<i class="icon-user"></i>
			</div>
			<div class="details">
				<div class="number">
					 Client
				</div>
				<div class="desc">
					Message
				</div>
			</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light purple-soft" href="{{ route('app_url.index') }}">
			<div class="visual">
				<i class="icon-grid"></i>
			</div>
			<div class="details">
				<div class="number">
					 Application
				</div>
				<div class="desc">
					 URL
				</div>
			</div>
			</a>
		</div>
	</div>
	<br><br>
	<div class="row cover">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light purple-soft" href="{{ route('slider.index') }}">
			<div class="visual">
				<i class="icon-screen-desktop"></i>
			</div>
			<div class="details">
				<div class="number">Slider</div>
				<div class="desc">Image</div>
			</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light green-soft" href="{{ route('contact_message.index')}}">
			<div class="visual">
				<i class="fa fa-plane"></i>
			</div>
			<div class="details">
				<div class="number">
					Contact
				</div>
				<div class="desc">
					Message
				</div>
			</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light red-soft" href="{{ route('client_message.index') }}">
			<div class="visual">
				<i class="fa fa-users"></i>
			</div>
			<div class="details">
				<div class="number">
					 Client
				</div>
				<div class="desc">
					Message
				</div>
			</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-light blue-soft" href="{{ route('board.index') }}">
			<div class="visual">
				<i class="fa fa-plane"></i>
			</div>
			<div class="details">
				<div class="number">
					 Board
				</div>
				<div class="desc">
					 Content
				</div>
			</div>
			</a>
		</div>
	</div>
	<!-- END DASHBOARD STATS -->
	<div class="clearfix">
	</div><br><br>
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12">
			
		</div>
	</div>
@endsection

@section('script')

@endsection
