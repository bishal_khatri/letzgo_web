@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('public/frontend/css/flaticon.css')}}"/>
@endsection

@section('title' , 'Board Listing');

@section('add-button')
@can('add-slider')
@endcan
@endsection

@section('content')
<div class="portlet-body">
	
	<!-- card section -->
		<div class="card-section">
			<div class="container">
				<div class="row">
					<div class="col-md-3  col-md-offset-1">
						<div class="lab-card">
							<div class="icon">
								<i style="font-size: 100px;" class="{{ $board1->icon }}"></i>
							</div>
							<h2>{{ $board1->title }}</h2>
							<p> {{ $board1->subtitle }}</p>
						</div>

					</div>
					
					<div class="col-md-3 ">
						<div class="lab-card">
							<div class="icon">
								<i style="font-size: 100px;"  class="{{  $board2->icon }}"></i>
							</div>
							<h2> {{ $board2->title }}</h2>
							<p> {{ $board2->subtitle }}</p>
						</div>

					
					</div>
					<!-- single card -->
					<div class="col-md-3 col-sm-12">
						<div class="lab-card">
							<div class="icon">
								<i style="font-size: 100px;" class="{{ $board3->icon }}"></i>
							</div>
							<h2> {{ $board3->title }}</h2>
							<p> {{ $board3->subtitle }}</p>
						</div>
					</div>
				</div>
			

			</div>

			<div class="row">
				<div class="col-md-4  ">
						<form role="form" action="{{ route('board.update') }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="control-label ">Icon</label>
							<input class="form-control placeholder-no-fix" type="text" placeholder="Enter Icon Syntax" name="icon" value="{{ $board1->icon }}"/>
						</div>

						<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
							<label class="control-label ">Title:</label>
							<input class="form-control placeholder-no-fix" type="text" placeholder="Enter Title:" name="title" value="{{ $board1->title }}"/>
						</div>

						<div class="form-group {{ $errors->has('subtitle') ? ' has-error' : '' }}">
							<label class="control-label ">Subtitle</label>
							<textarea class="form-control placeholder-no-fix" rows="8" type="text" placeholder="Enter content" name="subtitle" value="{{ old('subtitle') }}"/>{{ $board1->subtitle }}</textarea>
						</div>

						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-10">
									<input type="hidden" name="board" value="board1" />
						  			<button type="submit" class="btn green">Update</button>
						  			<button class="btn default" type="reset" >Cancel</button>
								</div>
							</div>
						</div>
					
					</form>
				</div>

				<div class="col-md-4 ">
						<form role="form" action="{{ route('board.update') }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="control-label ">Icon</label>
							<input class="form-control placeholder-no-fix" type="text" placeholder="Enter Icon Syntax" name="icon" value="{{ $board2->icon }}"/>
						</div>

						<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
							<label class="control-label ">Title:</label>
							<input class="form-control placeholder-no-fix" type="text" placeholder="Enter Title:" name="title" value="{{ $board2->title }}"/>
						</div>

						<div class="form-group {{ $errors->has('subtitle') ? ' has-error' : '' }}">
							<label class="control-label ">Subtitle</label>
							<textarea class="form-control placeholder-no-fix" rows="8" type="text" placeholder="Enter content" name="subtitle" />{{ $board2->subtitle }}</textarea>
						</div>

						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-10">
									<input type="hidden" name="board" value="board2" />
						  			<button type="submit" class="btn green">Update</button>
						  			<button class="btn default" type="reset" >Cancel</button>
								</div>
							</div>
						</div>
					
					</form>				
				</div>

				<div class="col-md-4 ">
						<form role="form" action="{{ route('board.update') }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="control-label ">Icon</label>
							<input class="form-control placeholder-no-fix" type="text" placeholder="Enter Icon Syntax" name="icon" value="{{ $board3->icon }}"/>
						</div>

						<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
							<label class="control-label ">Title:</label>
							<input class="form-control placeholder-no-fix" type="text" placeholder="Enter Title:" name="title" value="{{ $board3->title }}"/>
						</div>

						<div class="form-group {{ $errors->has('subtitle') ? ' has-error' : '' }}">
							<label class="control-label ">Subtitle</label>
							<textarea class="form-control placeholder-no-fix" rows="8" type="text" placeholder="Enter content" name="subtitle"  />{{ $board3->subtitle  }}</textarea>
						</div>

						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-10">
									<input type="hidden" name="board" value="board3" />
						  			<button type="submit" class="btn green">Update</button>
						  			<button class="btn default" type="reset" >Cancel</button>
								</div>
							</div>
						</div>
					
					</form>
					</div>
			</div>

		</div>
		<!-- card section end-->
  
</div>
@endsection	