@extends('layouts.app')

@section('css')
 <link href="{{ url('public/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ url('public/pages/css/profile.css')}}" rel="stylesheet" type="text/css" />
<style>
.terminated{
    background-color: rgba(189, 195, 91, 0.35) !important;
    color: #000 !important;
}
.profile-sidebar-portlet {
    padding: 30px 0 0 0 !important;
    border: 2px solid #f5f5f5;
}
</style>
@endsection

@section('title') Chart @endsection

@section('add-button')
@can('add-user')
{{-- <a href="{{ route('user.register') }}" class="btn btn-outline green"> <i class="icon-plus"></i> Add new </a> --}}
@endcan
@endsection

@section('content')
{{-- {{dd($user)}} --}}
<div class="portlet-body">
	<div class="row">
		<div  style="padding-left: 500px;" id="loading">
			<i class="fa fa-spinner fa-spin"></i> <span> Loading chart data....</span>
		</div>
	    <div class="col-md-12">
	       {{-- <div id="line_chart" style="width: 100%;height: 500px;"></div> --}}
	        {{-- <div id="curve_chart" style="width: 900px; height: 500px"></div> --}}
	          <div id="chart_div" style="width: 100%; height: 500px"></div>


		</div>
</div>


@endsection

@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
{{-- <script type="text/javascript">
	google.charts.load('current', {'packages': ['corechart']});
	google.charts.setOnLoadCallBack(drawChart);
	function drawChart(){
		var data = new google.visualization.DataTable(1110000);
		var options : {
			title : 'Sensors Data',
			legend: {position: 'bottom'},
			chartArea:{width:'95%', height:'65%'
		}

		};
		var chart = new google.visualization.LineChart(document.getElementById('line_chart'));
		chart.draw(data, options);
	}
</script> --}}
 <script type="text/javascript">
 $(document).ready(function() {
	$.ajax({
		type:'GET',
		url: "{{ route('chart.data') }}",
		success: function(response){
			$('#loading').fadeOut(2000);

			// var obj = jQuery.parseJSON(response);
			console.log(response);
			jQuery.each(response, function(index, item) {
    			
			});

			google.charts.load('current', {packages: ['line']});
			google.charts.setOnLoadCallback(drawChart);

			function drawChart() {
		      	var data = new google.visualization.DataTable();
		      	data.addColumn('date', 'Date');
		      	data.addColumn('number', 'ADC1');
		      	data.addColumn('number', 'Status');

		      	data.addRows([
		      		

		        		[new Date(2012, 1, 23), 213, 52],
		        		[new Date(2012, 1, 25), 250, 84],
		      	]);

		      	var options = {
		        	hAxis: {
		          		title: 'Date',
		          		format: 'M/d/yyy',
		            	gridlines: {count: 15}
		        	},
		        	vAxis: {
		          		title: 'Value'
		        	},
		        	series: {
		          		1: {curveType: 'function'}
		        	},
		        	line: {
		          		groupByRowLabel: true
		        	}
		      	};

		      	var chart =  new google.charts.Line(document.getElementById('chart_div'));
		      	chart.draw(data, google.charts.Line.convertOptions(options));
		    }
		}

	});

	
 	});
</script>

@endsection
