@extends('layouts.app')

@section('css')
 <link href="{{ url('public/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ url('public/pages/css/profile.css')}}" rel="stylesheet" type="text/css" />
<style>
.terminated{
    background-color: rgba(189, 195, 91, 0.35) !important;
    color: #000 !important;
}
.profile-sidebar-portlet {
    padding: 30px 0 0 0 !important;
    border: 2px solid #f5f5f5;
}
</style>
@endsection

@section('title') Chart @endsection

@section('add-button')
@can('add-user')
{{-- <a href="{{ route('user.register') }}" class="btn btn-outline green"> <i class="icon-plus"></i> Add new </a> --}}
@endcan
@endsection

@section('content')
{{-- {{dd($user)}} --}}
<div class="portlet-body">
	<div class="row">
	    <div class="col-md-12">
	       {{-- <div id="line_chart" style="width: 100%;height: 500px;"></div> --}}
	        {{-- <div id="curve_chart" style="width: 900px; height: 500px"></div> --}}
	          <div id="chart_div" style="width: 100%; height: 500px"></div>


		</div>
</div>


@endsection

@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
{{-- <script type="text/javascript">
	google.charts.load('current', {'packages': ['corechart']});
	google.charts.setOnLoadCallBack(drawChart);
	function drawChart(){
		var data = new google.visualization.DataTable(1110000);
		var options : {
			title : 'Sensors Data',
			legend: {position: 'bottom'},
			chartArea:{width:'95%', height:'65%'
		}

		};
		var chart = new google.visualization.LineChart(document.getElementById('line_chart'));
		chart.draw(data, options);
	}
</script> --}}
 <script type="text/javascript">
	google.charts.load('current', {packages: ['line']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {
      	var data = new google.visualization.DataTable();
      	data.addColumn('date', 'Date');
      	data.addColumn('number', 'ADC1');
      	data.addColumn('number', 'Status');

      	data.addRows([
      		@foreach($chartData as $key=>$val)
      		<?php
      			$date_explode = explode(' ', $val[0]);
		        $date = explode('-', $date_explode[0]);
		        $date_yy = $date[0];
		        $date_mm = $date[1];
		        $date_dd = $date[2];

		        $time = explode(':', $date_explode[1]);
		        $time_h = $time[0];
		        $time_m = $time[1];
		        $time_s = $time[2];
      		?>

        		[new Date({{ $date_yy }}, {{ $date_mm }}, {{ $date_dd }}), {{ $val['adc1'] }}, {{ $val['status'] }}],
        	@endforeach
      	]);

      	var options = {
        	hAxis: {
          		title: 'Date',
          		format: 'M/d/yyy',
            	gridlines: {count: 15}
        	},
        	vAxis: {
          		title: 'Value'
        	},
        	series: {
          		1: {curveType: 'function'}
        	},
        	line: {
          		groupByRowLabel: true
        	}
      	};

      	var chart =  new google.charts.Line(document.getElementById('chart_div'));
      	chart.draw(data, google.charts.Line.convertOptions(options));
    }
</script>

@endsection
