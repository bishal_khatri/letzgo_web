<div class="page-sidebar-wrapper">
    <div class="page-sidebar fixed-side-menu navbar-collapse collapse" style=" max-height: 100vh !important; padding-bottom: 100px;">
        <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
           <?php $left_menu = \App\Http\Controllers\ModuleController::listMenu();?>
            <li class="start @if(\Request::is('home/*')or \Request::is('home')) active @endif">
                <a href="{{ route('home') }}">
                <i class="icon-home" aria-hidden="true"></i>
                <span class="title text-uppercase">Dashboard</span>
                <span class="selected"></span>
                </a>
            </li>
            @foreach ($left_menu as $module=>$menus)
                <?php  $module = explode('_', $module); ?>
             {{-- {{ dd($module[0]) }} --}}
             {{-- {{ dd($menus) }} --}}
                @can($menus[0]['slug'])
                @if (!$module[2])
                    <li class="start @if(\Request::is('$module[0]/*')or \Request::is('module[0]')) active  @endif">
                        <a href="javascript:;">
                        <i class="{{ $module[1] }}"></i>
                        <span class="title text-uppercase">{{ $module[0] }}</span>
                        <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            @foreach($menus as $menu)
                            <?php  $full_link = $menu['module_link'].'/'.$menu['link']; ?>
                            <li>
                                <a href="{{ url(ADMIN.$full_link) }}">
                                <i class="{{ $menu['menu_icon'] }}"></i>
                                {{ $menu['menu_name'] }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li class="start @if(preg_match("/".$module[0]."/i",Request::path())) active @endif">
                        <a href="{{ url(ADMIN.$menus[0]['name'].'/'.$menus[0]['link']) }}">
                        <i class="{{ $menus[0]['icon'] }}"></i>
                        <span class="title text-uppercase">{{ $menus[0]['display_name'] }}</span>
                        <span class="selected"></span>
                        </a>
                    </li>
                @endif
                @endcan
            @endforeach
        </ul>
    </div>
</div>
