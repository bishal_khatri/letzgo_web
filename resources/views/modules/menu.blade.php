@extends('layouts.app')

@section('title') Menu @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
    <div class="portlet-body">
        <form role="form" action="{{ route('menu.store') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="module_id" id="" value="{{ $id }}">
            <input type="hidden" name="module_link" id="" value="{{ $name }}">
            <div class="form-body">
                <div class="form-group {{ $errors->has('menu_name') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Menu Name</label>
                <div class="input-icon">
                    <i class="fa fa-font" id="icon-defult"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Menu Name" name="menu_name" value="{{ old('menu_name') }}" />
                </div>
                 @if ($errors->has('menu_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('menu_name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-body">
                <div class="form-group {{ $errors->has('menu_icon') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Menu Icon</label>
                <div class="input-icon">
                    <i class="fa fa-bars" id="icon-defult"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Menu Icon" name="menu_icon" value="{{ old('menu_icon') }}" />
                </div>
                 @if ($errors->has('menu_icon'))
                    <span class="help-block">
                        <strong>{{ $errors->first('menu_icon') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-body">
                <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9"> Menu Link</label>
                <div class="input-icon">
                    <i class="fa fa-link" id="icon-defult"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Link " name="link" value="{{ old('link') }}" />
                </div>
                 @if ($errors->has('link'))
                    <span class="help-block">
                        <strong>{{ $errors->first('link') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-body">
                <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Slug </label>
                <div class="input-icon">
                    <i class="fa fa-anchor" id="icon-defult"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Slug " name="slug" value="{{ old('slug') }}" />
                </div>
                 @if ($errors->has('slug'))
                    <span class="help-block">
                        <strong>{{ $errors->first('slug') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-body">
                <div class="form-group {{ $errors->has('menu_order') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Menu Order</label>
                <div class="input-icon">
                    <i class="fa fa-sort" id="icon-defult"></i>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Menu Order " name="menu_order" value="{{ old('menu_order') }}" />
                </div>
                 @if ($errors->has('menu_order'))
                    <span class="help-block">
                        <strong>{{ $errors->first('menu_order') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group form-md-line-input">
                <label class="control-label" for="form_control_1"> Status</label>
                <div class="form-group form-md-line-input">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="radio3" name="show_hide" class="md-radiobtn" checked value="1">
                                <label for="radio3">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Show </label>
                            </div>
                            <div class="md-radio has-error">
                                <input type="radio" id="radio4" name="show_hide" class="md-radiobtn" value="0">
                                <label for="radio4">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Hide </label>
                            </div>
                        </div>
                </div>
            </div>
            </div>
            <div class="form-actions noborder">
                <div class="row">
                    <div class="col-md-10">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<div class="clearfix"></div>

@endsection