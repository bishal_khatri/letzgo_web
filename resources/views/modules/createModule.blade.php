@extends('layouts.app')

@section('title')
	Modules
@endsection
@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection
@section('content')
    <div class="portlet-body">
        <form role="form" action="{{ route('module.create') }}" method="post">
            {{ csrf_field() }}
            <div class="form-body">
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" name="display_name">
                    <label for="form_control_1">Display Name <span id="icon-terminate">*</span></label>
                    <span class="help-block">Enter Display Name</span>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" name="name">
                    <label for="form_control_1">Module Name<span id="icon-terminate">*</span></label>
                    <span class="help-block">Enter Module Name</span>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" name="table_name">
                    <label for="form_control_1">Table Name</label>
                    <span class="help-block">Enter Table Name</span>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" name="icon">
                    <label for="form_control_1">Icon<span id="icon-terminate">*</span></label>
                    <span class="help-block">Enter Icon</span>
                </div>
                <div class="form-group form-md-line-input form-md-floating-label">
                    <input type="text" class="form-control" name="order">
                    <label for="form_control_1">Order</label>
                    <span class="help-block">Enter Display Order</span>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="control-label" for="form_control_1"> Single Menu</label>
                    <div class="form-group form-md-line-input">
                            <div class="md-radio-inline">
                                <div class="md-radio">
                                    <input type="radio" id="radio1" name="is_single" class="md-radiobtn" checked value="1">
                                    <label for="radio1">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Yes </label>
                                </div>
                                <div class="md-radio has-error">
                                    <input type="radio" id="radio2" name="is_single" class="md-radiobtn" value="0">
                                    <label for="radio2">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    No </label>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="control-label" for="form_control_1"> Status</label>
                    <div class="form-group form-md-line-input">
                            <div class="md-radio-inline">
                                <div class="md-radio">
                                    <input type="radio" id="radio3" name="status" class="md-radiobtn" checked value="1">
                                    <label for="radio3">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Show </label>
                                </div>
                                <div class="md-radio has-error">
                                    <input type="radio" id="radio4" name="status" class="md-radiobtn" value="0">
                                    <label for="radio4">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Hide </label>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="form-actions noborder">
                <div class="row">
                    <div class="col-md-10">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<div class="clearfix"></div>

@endsection