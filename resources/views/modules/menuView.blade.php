@extends('layouts.app')

@section('title') View Menu @endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('public/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}" />
@endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="data-table">
        <thead>
            <tr>
                <th>#</th>
                <th> Display Name</th>
                <th> Module Link</th>
                <th> Menu Link</th>
                <th> Menu Order</th>
                <th> Status</th>
                <th style="width: 40px;"> Action</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($menu))
            @foreach ($menu as $val)
            <tr class="odd gradeX">
                <td>{{ $val->id }}</td>
                <td>{{ $val->menu_name }}</td>
                <td>{{ $val->module_link }}</td>
                <td>{{ $val->link }}</td>
                <td>{{ $val->menu_order }}</td>
                <td>@if( $val->show_hide==1 ) <span class="label label-success">Visible</span> @else <span class="label label-danger">Hidden</span> @endif</td>                         
                <td>
                    <div class="btn-group pull-right">
                        <button class="btn btn-defult" data-toggle="dropdown"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#"><i class="fa fa-edit icon" id="icon-defult" ></i> Edit</a></li>
                            <li><a href="#confirmDelete"><i class="fa fa-trash" id="icon-terminate" ></i> Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('public/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>

<script>
    var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#data-table');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": false }
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': true,
                'targets': [5]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [4, "asc"]
            ] // set first column as a default sort by asc
        });
    }
    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }
    };

}();  

jQuery(document).ready(function() {       
   // Metronic.init(); // init metronic core components
    // Layout.init(); // init current layout
    // Demo.init(); // init demo features
   TableManaged.init();
});     
</script>


@endsection