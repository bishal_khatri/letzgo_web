@extends('layouts.app')

@section('css')
@endsection

@section('title') Modules @endsection

@section('add-button')
<a href="{{ route('module.create') }}" class="btn btn-outline green"> <i class="icon-plus"></i> Add new </a>
@endsection

@section('content')
<div class="portlet-body">
    {{-- <div class="table-toolbar">
        <div class="row">
            <div class="col-md-6 pull-right">
                <div class="btn-group pull-right">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="javascript:;"> Print </a> </li>
                        <li> <a href="javascript:;"> Save as PDF </a> </li>
                        <li> <a href="javascript:;"> Export to Excel </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> --}}
    <table class="table table-striped table-bordered table-hover" id="data-table">
        <thead>
            <tr>
                <th> # </th>
                <th> Display Name </th>
                <th> Name </th>
                <th> Table Name </th>
                <th> Status </th>
                <th style="width: 75px;"> Action </th>
            </tr>
        </thead>
        <tbody>
            @if(isset($module))
            @foreach ($module as $val)
            <tr class="odd gradeX">
                <td> {{ $val->id }} </td>
                <td> {{ $val->display_name }} </td>
                <td> {{ $val->name }} </td>
                <td> {{ $val->table_name }} </td>
                <td> @if( $val->status==1 ) <span class="label label-success">Visible</span> @else <span class="label label-danger">Hidden</span> @endif</td>
                <td class="center">
                    <div class="btn-group pull-right">
                        <button class="btn btn-defult" data-toggle="dropdown"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('menu.create',[$val->id,$val->name]) }}">
                                    <i class="icon-plus icon" id="icon-defult"></i> Add Menu </a>
                            </li>
                             <li>
                                <a href="{{ route('menu.view',$val->id) }}">
                                    <i class="icon-list icon" id="icon-defult"></i> View Menu </a>
                            </li>
                            <li>
                                <a href="{{ route('module.edit',$val->security_key) }}">
                                    <i class="fa fa-edit icon" id="icon-defult" ></i> Edit </a>
                            </li>
                            
                            <li><a href="#confirmDelete" data-ids="{{ $val->security_key }}"
                                                   data-user="{{ $val->name or '' }}" data-rel="delete"
                                                   data-toggle="modal"
                                                   data-title="Delete {{$val->name or '' }}"
                                                   data-message='Are you sure you want to delete this row ?'->
                                <i class="fa fa-trash" id="icon-terminate" ></i> Delete </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@include('modules.delete_modal')
@endsection


@section('script')
<script>

var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#data-table');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": false }
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': true,
                'targets': [5]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
    }
    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }
    };

}();
jQuery(document).ready(function() {       
   // Metronic.init(); // init metronic core components
    // Layout.init(); // init current layout
    // Demo.init(); // init demo features
   TableManaged.init();
});

// AJAX DELETE START
    $('#confirmDelete').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#hidden_id").val(ids);
    $(".hidden_title").html(' "' + user + '" ');
});

$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#hidden_id").val();
    // console.log(id);
    $.ajax({
        type: "POST",
        url: "{{ route('module.delete') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            // console.log(msg);
            $("#confirmDelete").modal("hide");
            window.location.reload();
        }
    });
});

// DELETE END
</script>
@endsection