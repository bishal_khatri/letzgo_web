@extends('layouts.app')

@section('title') Update @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
<div class="portlet-body">
    <form class="register-form" action="{{ route('module.edit',$data->security_key) }}" method="post">
        {{ csrf_field() }}
        <div class="form-group {{ $errors->has('display_name') ? ' has-error' : '' }}">
            <label class="control-label">Display Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Display Name" name="display_name" value="{{ $data->display_name or '' }}" />
            </div>
            @if ($errors->has('display_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('display_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label">Name</label>
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Name" name="name" value="{{ $data->name or '' }}" />
            </div>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('table_name') ? ' has-error' : '' }}">
            <label class="control-label">Table Name</label>
            <div class="input-icon">
                <i class="fa fa-phone"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Table Name" name="table_name" value="{{ $data->table_name or '' }}" />
            </div>
            @if ($errors->has('table_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('table_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
            <label class="control-label">Icon</label>
            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Icon" name="icon" value="{{ $data->icon or '' }}" />
            </div>
            @if ($errors->has('icon'))
                <span class="help-block">
                    <strong>{{ $errors->first('icon') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('order') ? ' has-error' : '' }}">
            <label class="control-label">Order</label>
            <div class="input-icon">
                <i class="fa fa-location-arrow"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="order" name="city" value="{{ $data->order or '' }}" />
            </div>
            @if ($errors->has('order'))
                <span class="help-block">
                    <strong>{{ $errors->first('order') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group form-md-line-input">
            <label class="control-label" for="form_control_1"> Single Menu</label>
            <div class="form-group form-md-line-input">
                <div class="md-radio-inline">
                    <div class="md-radio">
                        <input type="radio" id="radio1" name="is_single" class="md-radiobtn" @if($data->is_single==1) checked @else '' @endif value="1">
                        <label for="radio1">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span>
                        Yes </label>
                    </div>
                    <div class="md-radio has-error">
                        <input type="radio" id="radio2" name="is_single" class="md-radiobtn" @if($data->is_single==0) checked @else '' @endif value="0">
                        <label for="radio2">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span>
                        No </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group form-md-line-input">
            <label class="control-label" for="form_control_1"> Status</label>
            <div class="form-group form-md-line-input">
                    <div class="md-radio-inline">
                        <div class="md-radio">
                            <input type="radio" id="radio3" name="status" class="md-radiobtn" @if($data->status==1) checked @else '' @endif value="1">
                            <label for="radio3">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span>
                            Show </label>
                        </div>
                        <div class="md-radio has-error">
                            <input type="radio" id="radio4" name="status" @if($data->status==0) checked @else '' @endif class="md-radiobtn" value="0">
                            <label for="radio4">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span>
                            Hide </label>
                        </div>
                    </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-8">
                   <button type="submit" class="btn green">Submit</button>
                    <a class="btn default" href="{{ URL::previous() }}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
