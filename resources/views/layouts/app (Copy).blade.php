<!DOCTYPE html>
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>AdminPanel - @yield('title')</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ asset('global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
{{-- <link href="{{ asset('global/plugins/fontawesome-5/web-fonts-with-css/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css"/> --}}
<link href="{{ asset('global/plugins/fontawesome-5/web-fonts-with-css/css/fa-regular.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="{{ asset('global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css"/>
{{-- <link href="{{ asset('global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css"/> --}}
<link href="{{ asset('global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="{{ asset('layout2/tasks.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="{{ asset('global/css/components-md.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('global/css/plugins-md.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('layout2/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('layout2/css/themes/grey.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{ asset('layout2/css/custom.css')}}" rel="stylesheet" type="text/css"/>
@yield('css')
<!-- END THEME STYLES -->
{{-- <link rel="shortcut icon" href="favicon.ico"/> --}}
<style>
    .actions{
        margin-top: 20px;
    }
    .portlet-body{
        min-height: 40px;
    }

    .form-actions{
        padding-top: 40px;
    }
    
    .control-label{
        text-align: right;
    }
    .portlet > .portlet-title > .tools > a {
        margin-top: -10px;
    display: inline-block;
    height: 35px;
    margin-left: 5px;
    opacity: 1;
    filter: alpha(opacity=100);
}

.page-header.navbar .page-logo {
    background: #951934;
}

.btn-group > .btn:first-child {
    border: 1px solid #19957A !important;
    color: #19957A;
    margin-left: 0;
    background-color:transparent;
}

.btn-group > .btn:first-child:hover {
    color: #fff;
    margin-left: 0;
    background-color: #19957A;
}



#icon-terminate{
    color: #951934;
}

#icon-defult{
    color: #19957A;
}

.page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.active > a > i, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.active.open > a > i, .page-sidebar .page-sidebar-menu > li.active > a > i, .page-sidebar .page-sidebar-menu > li.active.open > a > i{
    color: #951934;
}
   
</style>
</head>
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
    @include('includes.nav')
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	@include('includes.sidebar')
	 <div class="page-content-wrapper">
	 	<div class="page-content"> 
	 		<!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{route('home')}}"> Dashboard </a>
                    </li>
                    @if(isset($main_menu_name))
                    <i class="fa fa-angle-right"></i>
                    <li>
                        <a href="#"> {{ $main_menu_name }} </a>
                    </li>
                     @endif
                </ul>
            </div>
            @include('includes.flashMessage')
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption ">
                                <i class="icon-list font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> Insights </span>
                                <span class="caption-helper uppercase"> - {{ $page_title or '' }}</span>
                            </div>
                            <div class="tools">
                                @yield('add-button')
                            </div>
                            <div class="actions">
                                @yield('back-button')
                            </div>
                        </div>
    		            @yield('content')
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
    	    </div>
        </div>
    </div>
</div>
<!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner">
             2018 &copy; AdminPanel by Bishal.
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->


@include('includes.footer')