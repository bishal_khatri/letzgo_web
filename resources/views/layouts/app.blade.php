<!DOCTYPE html>
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>{{ env('APP_NAME','Admin') }} - @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/global/plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.min.css') }}" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{ asset('public/assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/admin/layout2/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{ asset('public/assets/admin/layout2/css/themes/grey.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('public/assets/admin/layout2/css/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link href="{{ asset('public/favicon.ico') }}" rel="shortcut icon"/>
@yield('css')
<!-- END THEME STYLES -->
{{-- <link rel="shortcut icon" href="favicon.ico"/> --}}
<style>
@font-face {
    font-family: quicksand;
    src: url(public/fonts/quicksand/Quicksand-Regular.otf);
}
@font-face {
    font-family: Muli-regular;
    src: {{ url('public\fonts\Muli\Muli-Regular.ttf')}};
}
@font-face {
    font-family: Muli-bold;
    src: {{ url('public\fonts\Muli\Muli-ExtraBold.ttf')}};
}
body {
    font-family: Muli-regular !important;
}
.page-sidebar-wrapper{
    font-family: Muli-bold !important;
}
.page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover, .page-sidebar,.page-sidebar a {
    background-color: #293641;
}

.page-sidebar .page-sidebar-menu > li > a > .title{
  font-weight: bold;
  font-size: 10px;
}
.page-content-wrapper {
    background-color: #293641;
}
.page-header.navbar .page-logo {
    background: #1a2229;
    z-index: 9999;
    position: fixed;
}

.page-bar{
  margin-top: 1px;
}

.page-header-fixed .page-container{margin-top: 0px !important}

.page-header.navbar .search-form{margin-left: 200px;position: relative;}

.page-content-wrapper .page-content{padding: 12px 20px 10px 20px;}

.page-footer{
  background: #fff;
}


    .fixed-side-menu {
        position: fixed;

    }
    .fixed-side-menu::-webkit-scrollbar {
        width: 2px;
    }

    .fixed-side-menu::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    }

    .fixed-side-menu::-webkit-scrollbar-thumb {
        background-color: darkgrey;
        outline: 1px solid slategrey;
    }

    .actions{
        margin-top: 20px;
    }
    .portlet-body{
        min-height: 40px;
    }

    .form-actions{
        padding-top: 40px;
    }

    .control-label{
        text-align: right;
    }
    .portlet > .portlet-title > .tools > a {
        margin-top: -10px;
    display: inline-block;
    height: 35px;
    margin-left: 5px;
    opacity: 1;
    filter: alpha(opacity=100);
    background-color: #14B9D6;
}

/*.page-header.navbar .page-logo {
    background: #87CEFF;
}*/

.btn-group > .btn:first-child {
    border: thin solid #14B9D6 !important;
    color: #14B9D6;
    margin-left: 0;
    background-color:transparent;
}

.btn-group > .btn:first-child:hover {
    color: #fff;
    margin-left: 0;
    background-color: #14B9D6;
}



#icon-terminate{
    color: #951934;
}

#icon-defult{
    color: #14B9D6;
}

/*.page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.active > a > i, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.active.open > a > i, .page-sidebar .page-sidebar-menu > li.active > a > i, .page-sidebar .page-sidebar-menu > li.active.open > a > i{
    color: #951934;
}*/

.navbar-collapse.collapse {
   z-index: 1;
}

.logo-default{

    margin-top: 20px !important;
    padding-left: 10px;
    font-weight: bold;
    color: #fff;
}



</style>
</head>
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
    @include('includes.nav')
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('includes.sidebar')
     <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{route('home')}}"> Dashboard </a>
                    </li>
                    @if(isset($main_menu_name))
                    <i class="fa fa-angle-right"></i>
                    <li>
                        <a href="#"> {{ $main_menu_name }} </a>
                    </li>
                     @endif
                </ul>
            </div>
            @include('includes.flashMessage')
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption ">
                                <i class="icon-list font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> Insights </span>
                                <span class="caption-helper uppercase"> - {{ $page_title or '' }}</span>
                            </div>
                            <div class="tools">
                                @yield('add-button')
                            </div>
                            <div class="actions">
                                @yield('back-button')
                            </div>
                        </div>
                        @yield('content')
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner pull-right">
             2018 &copy; Technology Sales: {{ env('APP_NAME','Admin') }}
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->


@include('includes.footer')

</body>
<!-- END BODY -->
</html>
