<div class="modal fade bs-modal-sm" id="confirmDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Terminate Permanently
                </h4>
            </div>
            <div class="modal-body"> Do you want to Terminate <span class='hidden_title'>" "</span>?</div>
            <input type="hidden" id="hidden_id">
            <div class="modal-footer">
                <button type="button" class="btn btn-defult green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                </button>
                <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                    No
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- suspend-user-modal -->
<div class="modal fade bs-modal-sm" id="confirmSuspend" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Suspend
                </h4>
            </div>
            <div class="modal-body"> Do you want to suspend <span class='s_hidden_title'>" "</span>?</div>
            <input type="hidden" id="s_hidden_id">
            <div class="modal-footer">
                <button type="button" class="btn btn-defult green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                </button>
                <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                    No
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- suspend-user-modal -->

<!-- restore-user-modal -->
<div class="modal fade bs-modal-sm" id="confirmRestore" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Restore
                </h4>
            </div>
            <div class="modal-body"> Do you want to restore <span class='r_hidden_title'>" "</span>?</div>
            <input type="hidden" id="r_hidden_id">
            <div class="modal-footer">
                <button type="button" class="btn btn-defult green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                </button>
                <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                    No
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- restore-user-modal -->