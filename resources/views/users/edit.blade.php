@extends('layouts.app')

@section('title') Update @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
<div class="portlet-body">
    <form class="register-form" action="{{ route('user.update',$data->security_key) }}" method="post">
        <p> Update your personal details below: </p>
        {{ csrf_field() }}
        <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Full Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="full_name" value="{{ $data->full_name or '' }}" />
            </div>
            @if ($errors->has('full_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('full_name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email" value="{{ $data->email or '' }}" />
            </div>
        </div>
        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Phone</label>
            <div class="input-icon">
                <i class="fa fa-phone"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Phone" name="phone" value="{{ $data->phone or '' }}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Address</label>
            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="address" value="{{ $data->address or '' }}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">City/Town</label>
            <div class="input-icon">
                <i class="fa fa-location-arrow"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="city" value="{{ $data->city or '' }}" />
            </div>
        </div>
        <p> Click <a href="#" id="icon-terminate">here</a> to change your account details: </p>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-8">
                   <button type="submit" class="btn green">Submit</button>
                    <a class="btn default" href="{{ URL::previous() }}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
