@extends('layouts.app')

@section('css')
<style>
.terminated{
    background-color: rgba(189, 195, 91, 0.35) !important;
    color: #000 !important;
}
</style>
@endsection

@section('title') Users @endsection

@section('add-button')
@can('add-user')
<a href="{{ route('user.register') }}" class="btn btn-outline green"> <i class="icon-plus"></i> Add new </a>
@endcan
@endsection

@section('content')
<div class="portlet-body">
   <table class="table table-striped table-bordered table-hover" id="data-table">
        <thead>
            <tr>
                <th>#</th>
                <th> Name</th>
                <th> User Name</th>
                <th> Contact</th>
                <th> Image</th>
                <th style="width: 40px;"> Action</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($users))
            @foreach ($users as $val)
            <tr @if($val->active == 0) class="terminated" @endif>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $val->full_name }}</td>
                <td>{{ $val->user_name }}</td>
                <td>
                    <small><strong>Email:</strong> {{ $val->email or ''}}<br></small>
                    <small><strong>Phone:</strong> {{ $val->phone or ''}} <br></small>
                    <small><strong>Address:</strong> {{ $val->address or ''}} <br></small>
                    <small><strong>City:</strong> {{ $val->city or ''}} <br></small>
                </td>
                <td class="center">
                    @if(isset($val->profile_image))
                        <img src="{{ asset("public/uploads/profile/$val->profile_image") }}" style="max-height: 70px">
                    @else
                        <img src="{{ url('public/uploads/icons/insert-images.png') }}" style="max-height: 70px">
                    @endif

                </td>
                <td>
                    <div class="btn-group pull-right">
                        <button class="btn btn-defult" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            @can('edit-user')
                            <li><a href="{{ route('user.edit',$val->security_key) }}">
                                <i class="fa fa-edit icon" id="icon-defult" ></i> Edit </a>
                            </li>
                            @endcan
                            @if($val->active==0)
                            @can('suspend-user')
                            <li><a href="#confirmRestore" data-ids="{{ $val->security_key }}"
                                                   data-user="{{ $val->full_name or '' }}" data-rel="delete"
                                                   data-toggle="modal"
                                                   data-title="Restore {{$val->full_name or '' }}"
                                                   data-message='Are you sure you want to restore this row ?'>
                                <i class="fa fa-refresh icon" id="icon-defult" ></i> Restore </a>
                            </li>
                            @endcan
                            @else
                            @can('suspend-user')
                            <li><a href="#confirmSuspend" data-ids="{{ $val->security_key }}"
                                                   data-user="{{ $val->full_name or '' }}" data-rel="delete"
                                                   data-toggle="modal"
                                                   data-title="Suspend {{$val->full_name or '' }}"
                                                   data-message='Are you sure you want to suspend this row ?'>
                                <i class="fa fa-times" id="icon-terminate"></i> Suspend </a>
                            </li>
                            @endcan
                            @endif
                            @can('delete-user')
                            <li><a href="#confirmDelete" data-ids="{{ $val->security_key }}"
                                                   data-user="{{ $val->full_name or '' }}" data-rel="delete"
                                                   data-toggle="modal"
                                                   data-title="Delete {{$val->full_name or '' }}"
                                                   data-message='Are you sure you want to terminate this row ?'->
                                <i class="fa fa-trash" id="icon-terminate" ></i> Terminate </a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@include('users.delete_modal')
@endsection

@section('script')
<script>
   // data table script
var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#data-table');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": true },
                {"orderable": false },
                {"orderable": false }
            ],
            "lengthMenu": [
                [-1, 5, 15, 20],
                ["All", 5, 15, 20] // change per page values here
            ],
            // set the initial value
            "pageLength": -1,
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                // 'orderable': true,
                // 'targets': [4]
            }, {
                // "searchable": false,
                // "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
    }
    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }
    };

}();
jQuery(document).ready(function() {
   TableManaged.init();
});
// data table script end

// AJAX DELETE USER
$('#confirmDelete').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#hidden_id").val(ids);
    $(".hidden_title").html(' "' + user + '" ');
});

$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#hidden_id").val();
    // console.log(id);
    $.ajax({
        type: "POST",
        url: "{{ route('user.delete') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            // console.log(msg);
            $("#confirmDelete").modal("hide");
            window.location.reload();
        }
    });
});
// AJAX DELETE USER END

// AJAX SUSPEND USER
$('#confirmSuspend').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#s_hidden_id").val(ids);
    $(".s_hidden_title").html(' "' + user + '" ');
});

$('#confirmSuspend').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#s_hidden_id").val();
    // console.log(id);
    $.ajax({
        type: "POST",
        url: "{{ route('user.suspend') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            // console.log(msg);
            $("#confirmSuspend").modal("hide");
            window.location.reload();
        }
    });
});
// AJAX SUSPEND USER END

// AJAX RESTORE USER
$('#confirmRestore').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#r_hidden_id").val(ids);
    $(".r_hidden_title").html(' "' + user + '" ');
});

$('#confirmRestore').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#r_hidden_id").val();
    // console.log(id);
    $.ajax({
        type: "POST",
        url: "{{ route('user.restore') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            // console.log(msg);
            $("#confirmRestore").modal("hide");
            window.location.reload();
        }
    });
});
// AJAX RESTORE USER END
</script>
@endsection
