@extends('layouts.app')
@section('title') 403 @endsection
	@section('css')
			<style media="screen">
			.portlet-body{
				padding-top: 100px !important;
				text-align: center;
				min-height: 400px !important;
			}
			h2{
				color: orange;
			}
			h1{
				letter-spacing: 5px;
				color: red;
			}
			</style>
	@endsection
@section('content')
	<div class="portlet-body">
		<h1>403</h1>
		<h2>Ooops!! we have a problem.</h2>
		<p>
			 You dont have access to view this page.
		</p>
		<p>
			<a href="{{ route('home') }}">
			Return Home </a>
			<br>
		</p>
	</div>
@endsection
