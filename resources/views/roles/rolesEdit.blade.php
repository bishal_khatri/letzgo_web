@extends('layouts.app')

@section('css')
   {{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.7.0/combined/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.7.0/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}
@endsection

@section('title') Roles @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')

<div class="portlet-body">
    <form action="{{ route('role.edit',$roles->id) }}" method="post">
        {{ csrf_field() }}
        <div class="form-group {{ $errors->has('roleName') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Role Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Role Name" name="roleName" value="{{ $roles->name or '' }}" />
            </div>
             @if ($errors->has('roleName'))
                <span class="help-block">
                    <strong>{{ $errors->first('roleName') }}</strong>
                </span>
            @endif
        </div>
        <span class="caption-subject font-green"><h4>Permissions</h4></span>
        <div class="form-group">
            <ul class="list-group">
                <li class="list-group-item">
                    {{-- <label><input type="checkbox"><strong>Module Title</strong> </label> --}}
                    {{-- {{dd($perm)}} --}}
                    @if(isset($permissions))
                    @foreach($permissions as $permission)
                    <ul>
                       <label>
                        <input type="checkbox" name="permission[]" value="{{ $permission['slug'] }}" @if(in_array($permission->slug,$perm)) checked @endif > {{ $permission['name'] }} 
                        </label>
                    </ul>
                    @endforeach
                    @endif
                </li>
            </ul>
            {{-- <ul class="list-group">
                <li class="list-group-item">
                    <label><input type="checkbox"><strong>Module Title</strong> </label>
                    <ul>
                       <label><input type="checkbox"> Permission </label>
                    </ul>
                </li>
            </ul> --}}
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-8">
                   <button type="submit" class="btn green">Update</button>
                            <a class="btn default" href="{{ URL::previous() }}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')

@endsection