// AJAX DELETE TEAM
$('#confirmDelete').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    //console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#hidden_id").val(ids);
    $(".hidden_title").html(' "' + user + '" ');
});
<!-- Form confirm (yes/ok) handler, submits form -->
$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#hidden_id").val();

    $.ajax({
        type: "POST",
        url: "delete",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            $("#confirmDelete").modal("hide");
            $('#tr_' + id).fadeOut(300, 0, function () {
                $(this).remove();
            });
            $("#message_container").append("<div class='alert alert-success'>News has been successfully deleted. </div>");
            setTimeout(function () {
                $(".alert").slideUp(500)
            }, 5000);
            //window.location.reload();
        }
    });




});