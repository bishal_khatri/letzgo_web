@extends('layouts.app')

@section('title') Roles @endsection

@section('back-button')
    <a href="{{ URL::previous() }}" class="btn btn-outline green"> <i class="fa fa-reply"></i> Back</a>
@endsection

@section('content')
<div class="portlet-body">
    <form action="{{ route('role.add') }}" method="post">
        {{ csrf_field() }}
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Full Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Role Name" name="name" value="{{ $data->name or '' }}" />
            </div>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-8">
                   <button type="submit" class="btn green">Submit</button>
                    <a class="btn default" href="{{ URL::previous() }}">Cancel</a>
                </div>
            </div>
        </div>
   </form>
</div>
@endsection