@extends('layouts.app')

@section('css')
@endsection

@section('title') Roles @endsection

@section('add-button')
  @can('add-role')
<a href="{{ route('role.add') }}" class="btn btn-outline green"> <i class="icon-plus"></i> Add new </a>
@endcan
@endsection

@section('content')
<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <thead>
            <tr>
                <th>#</th>
                <th> Name</th>
                <th style="width: 40px;"> Action</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($roles))
            @foreach ($roles as $val)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $val->name }}</td>
                <td>
                    <div class="btn-group pull-right">
                        <button class="btn btn-defult" data-toggle="dropdown"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                          @can('edit-role')
                            <li><a href="{{ route('role.edit',$val->id) }}">
                                <i class="fa fa-edit icon" id="icon-defult" ></i> Edit </a>
                            </li>
                          @endcan
                          @can('delete-role')
                            <li><a href="#confirmDelete" data-ids="{{ $val->security_key }}"
                                                   data-user="{{ $val->name or '' }}" data-rel="delete"
                                                   data-toggle="modal"
                                                   data-title="Delete {{$val->name or '' }}"
                                                   data-message='Are you sure you want to delete this row ?'->
                                <i class="fa fa-trash" id="icon-terminate" ></i> Delete </a>
                            </li>
                          @endcan
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@include('roles.delete_modal')
@endsection
@section('script')
<script>
// AJAX DELETE TEAM
$('#confirmDelete').on('show.bs.modal', function (e) {
    //e.preventDefault();

    var button = $(e.relatedTarget);
    // console.log(button.data('ids'));
    var ids = button.data('ids');
    var user = button.data('user');

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
    $("#hidden_id").val(ids);
    $(".hidden_title").html(' "' + user + '" ');
});

$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
    //$(this).data('form').submit();
    var id = $("#hidden_id").val();
    // console.log(id);
    $.ajax({
        type: "POST",
        url: "{{ route('role.delete') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: "id=" + id,
        success: function (msg) {
            console.log(msg);
            $("#confirmDelete").modal("hide");
            window.location.reload();
        }
    });
});
// AJAX DELETE TEAM END

// data table script
var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#data-table');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [
                {"orderable": true },
                {"orderable": false }
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': true,
                // 'targets': [2]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
    }
    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }
    };

}();
jQuery(document).ready(function() {
   TableManaged.init();
});
// data table script end
</script>
@endsection
