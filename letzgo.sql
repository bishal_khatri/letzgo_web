-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: letzgo_web
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_message`
--

DROP TABLE IF EXISTS `client_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(45) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `message` text,
  `unique_key` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_message`
--

LOCK TABLES `client_message` WRITE;
/*!40000 ALTER TABLE `client_message` DISABLE KEYS */;
INSERT INTO `client_message` VALUES (2,'Vishal Khatri','Manager','public/uploads/client_message/20180718180046.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consectetur consequatur distinctio maiores nemo qui. Ab asperiores explicabo hic impedit labore, modi similique? Dolore labore, nemo optio quod saepe voluptate!','dfgfdgdf','2018-07-18 12:15:46','2018-07-18 12:15:46'),(3,'Apple ball','Owner','public/uploads/client_message/20180718180310.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consectetur consequatur distinctio maiores nemo qui. Ab asperiores explicabo hic impedit labore, modi similique? Dolore labore, nemo optio quod saepe voluptate!','SWriencpVfnyQ2GRX0QI2FUhTjGNqkdDr7dr143bfzMvP','2018-07-18 12:18:10','2018-07-18 12:18:10'),(4,'Anna doe','Developer','public/uploads/client_message/20180718180843.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid consectetur corporis cupiditate maxime nihil quibusdam reprehenderit sed sint voluptatibus? Assumenda corporis eius laboriosam numquam omnis quibusdam quo quod, soluta tenetur!','jXZy1ax2UkHCepIaJQd22xubfJblnuEKEBTGP2GOKKBtL','2018-07-18 12:23:43','2018-07-18 12:23:43'),(5,'John Doe','Developer','public/uploads/client_message/20180718180920.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid consectetur corporis cupiditate maxime nihil quibusdam reprehenderit sed sint voluptatibus? Assumenda corporis eius laboriosam numquam omnis quibusdam quo quod, soluta tenetur!','nz7od7YhbUjP2pm3rLzelFY5q9S6reoVzOedQFsgx6WNu','2018-07-18 12:24:20','2018-07-18 12:24:20');
/*!40000 ALTER TABLE `client_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms`
--

DROP TABLE IF EXISTS `cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(45) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` text,
  `cover_image` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms`
--

LOCK TABLES `cms` WRITE;
/*!40000 ALTER TABLE `cms` DISABLE KEYS */;
INSERT INTO `cms` VALUES (1,'video',NULL,NULL,'public/uploads/video_cover/20180718165243.jpg','https://www.youtube.com/watch?v=WISIkcLM1Wk','2018-07-18 11:06:43','2018-07-20 04:58:22'),(2,'app_url','Are you ready to stand out?Edited','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est.Edited',NULL,'https://www.youtube.com/watch?v=HNgJPIwg5Mc&index=12&list=RDMMWXfxb093l_Q','2018-07-20 05:14:53','2018-07-20 05:43:04');
/*!40000 ALTER TABLE `cms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_messages`
--

DROP TABLE IF EXISTS `contact_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `client_ip` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_messages`
--

LOCK TABLES `contact_messages` WRITE;
/*!40000 ALTER TABLE `contact_messages` DISABLE KEYS */;
INSERT INTO `contact_messages` VALUES (1,'Bishal Khatri','bishal.khatri343@gmail.com',NULL,'This is a test message',NULL,'2018-07-20 07:16:24','2018-07-20 07:16:24'),(2,'Bishal Khatri','bishal.khatri343@gmail.com',NULL,'This is a test message','127.0.0.1','2018-07-20 07:18:47','2018-07-20 07:18:47');
/*!40000 ALTER TABLE `contact_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `menu_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `show_hide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_module_id_foreign` (`module_id`),
  CONSTRAINT `menus_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,1,'list','icon-list','module','list','list-module',1,'1',1,NULL,NULL),(2,2,'list','icon-list','user','list','list-user',1,'1',2,NULL,NULL),(3,3,'list','icon-list','role','list','list-role',1,'1',3,NULL,NULL),(4,4,'list','icon-list','cms','list','list-cms',1,'1',NULL,'2018-07-13 10:00:58','2018-07-13 10:00:58');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_12_03_110326_create_roles_table',1),(4,'2017_12_05_061139_create_modules_table',1),(5,'2017_12_05_092539_create_menus_table',1),(6,'2017_12_22_054417_create_role_user_table',1),(7,'2017_12_22_134824_create_permissions_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `security_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_single` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Modules','module','modules','icon-grid',1,1,'modulekey',1,NULL,NULL),(2,'Users','user','users','icon-user',2,1,'userkey',1,NULL,NULL),(3,'Roles','role','roles','icon-lock',3,1,'roleskey',1,NULL,NULL),(4,'Slider','slider',NULL,'icon-screen-desktop',5,1,'YQ2mnBr4dmCbJucChe24ZyVOmQ3Br3u1Y1KCtQpzofZke',1,'2018-07-13 10:00:35','2018-07-13 10:00:35');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'User List','list-user',NULL,NULL),(2,'User Add','add-user',NULL,NULL),(3,'User Edit','edit-user',NULL,NULL),(4,'User Delete','delete-user',NULL,NULL),(5,'Roels Add','add-role',NULL,NULL),(6,'Roles List','list-role',NULL,NULL),(7,'Roles Edit','edit-role',NULL,NULL),(8,'Roles Delete','delete-role','2016-12-31 23:20:05',NULL),(9,'Module List','list-module',NULL,NULL),(10,'Module Add','add-module',NULL,NULL),(11,'Module Edit','edit-module',NULL,NULL),(12,'Module Delete','delete-module',NULL,NULL),(27,'User Suspend','suspend-user',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `roles_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_roles_id_foreign` (`roles_id`),
  CONSTRAINT `role_user_roles_id_foreign` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,2,2,NULL,NULL);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `security_key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Editor','editor','{\"list-user\":true}','hzcnMoRWElYjK1puu3bka8ll61wwsU5pe2DJI3gT4wM2666cht','2018-01-18 07:59:20','2018-07-06 07:22:12'),(2,'Administrator','administrator','{\"list-user\":true,\"add-user\":true,\"edit-user\":true,\"delete-user\":true,\"list-flight\":true,\"list-all-flight\":true,\"add-flight\":true,\"edit-flight\":true,\"assign-flight\":true,\"change-status-flight\":true,\"airport-list\":true,\"suspend-user\":true}','dhemuOEVkbhijGTf5FsSKUAF1iKYhyOTSoHY87opneSonK74PQ','2018-03-14 04:29:26','2018-03-26 11:43:47');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `body` varchar(255) NOT NULL,
  `unique_key` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Get in the lab','flaticon-023-flask','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..','m2yYvsB6JEAKapvi04fsTaAtjQLBpQIdN7Y4mBxOoVSNU','2018-07-18 10:03:42','2018-07-18 10:03:42'),(2,'Projects online','flaticon-011-compass','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..','77kKLxvf5XLHXP7Cx5aRlEN8SRPulfne4NxeoxNS4dT0G','2018-07-18 10:04:17','2018-07-18 10:04:17'),(3,'SMART MARKETING','flaticon-037-idea','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..','FRolUcGT4xgm6z8ZLm41TZQ3ACUfVZ3UiXyBZUm3FWzB2','2018-07-18 10:04:39','2018-07-18 10:04:39'),(4,'Social Media','flaticon-039-vector','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..','SZzxXjca3bWl23q1MtSjSfXy9NTcBNzHhVo5JNPYyHgmT','2018-07-18 10:05:05','2018-07-18 10:05:05');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `unique_key` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (5,'public/uploads/slider/20180713161226.jpg','AgpAyBiqxfe7I7oObrpkTsorplrT2hnWEgBoZ3LE4BScq','2018-07-13 10:27:26','2018-07-13 10:27:26'),(6,'public/uploads/slider/20180713161230.jpg','IMjmfIZdKSaeqzXZsTXwvQBBPR0sLm7Yf8YGFhLnluL3J','2018-07-13 10:27:30','2018-07-13 10:27:30'),(7,'public/uploads/slider/20180715120834.jpg','TkNAM0BgOQRZPSqecLLn6mfPip19QxZjsLx1gho0pSGYt','2018-07-15 06:23:34','2018-07-15 06:23:34');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `real_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `security_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Super Admin',NULL,'superadmin@gmail.com','98756321452','kathmandu','kathmandu','superadmin','$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK','password','127.0.0.1','1','superadmin','WNAjpGnil6E7OArZdaSYWjbU680bepzE6w2ZN6au7RL4yungACSe9tOtprg5',NULL,'2017-12-28 11:19:48'),(2,'Bishal Khatri',NULL,'bishal.khatri343@gmail.com','687465465','Kathmandu','Kathmandu','bishal','$2y$10$SKOEnAkl3Pbc0ZWoaLhM.OapfYFErzatCYdtP1kwmrc2TC1xXR2Ni','password','172.16.1.123','1','4FSdtDH95wn0mlW1WpAldSeAK6GTbqULl83B8zba',NULL,'2018-07-13 09:14:56','2018-07-13 09:14:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-23 11:18:05
